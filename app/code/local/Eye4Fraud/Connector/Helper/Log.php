<?php

class Eye4Fraud_Connector_Helper_Log extends Mage_Core_Helper_Abstract
{
	public function log($message, $level = null)
	{
		Mage::log($message, $level, 'eye4fraud.log');
	}

    public function logException($e)
    {
        $this->log("Eye4Fraud extension had an exception: " . $e->getMessage());
    }
}