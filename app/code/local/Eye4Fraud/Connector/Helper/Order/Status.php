<?php

class Eye4Fraud_Connector_Helper_Order_Status extends Mage_Core_Helper_Abstract
{
    /**
     * Get Eye4fraud's custom "on hold for review" status code
     *
     * @return string
     */
    public function getOnHoldStatusCode()
    {
        return 'eye4fraud_holded';
    }

    /**
     * Get Eye4fraud's custom "on hold for review" status label
     *
     * @return string
     */
    public function getOnHoldStatusLabel()
    {
        return 'Under Review (Eye4fraud)';
    }

    /**
     * Get Eye4fraud's custom "on hold due to transport error" status code
     *
     * @return string
     */
    public function getTransportErrorStatusCode()
    {
        return 'eye4fraud_trans_error';
    }

    /**
     * Get Eye4fraud's custom "on hold due to transport error" status label
     *
     * @return string
     */
    public function getTransportErrorStatusLabel()
    {
        return 'Transport Error (Eye4fraud)';
    }

    /**
     * Get Eye4fraud's custom "declined" status code
     *
     * @return string
     */
    public function getEye4fraudDeclinedStatusCode()
    {
        return 'eye4fraud_declined';
    }

    /**
     * Get Eye4fraud's custom "declined" status label
     *
     * @return string
     */
    public function getEye4fraudDeclinedStatusLabel()
    {
        return 'Declined (Eye4fraud)';
    }

    /**
     * Get Eye4fraud's custom "fraud" status code
     *
     * @return string
     */
    public function getEye4fraudFraudStatusCode()
    {
        return 'eye4fraud_fraud';
    }

    /**
     * Get Eye4fraud's custom "fraud" status label
     *
     * @return string
     */
    public function getEye4fraudFraudStatusLabel()
    {
        return 'Fraud (Eye4fraud)';
    }

    /**
     * Get Eye4fraud's custom "insured" status code
     *
     * @return string
     */
    public function getEye4fraudInsuredStatusCode()
    {
        return 'eye4fraud_insured';
    }

    /**
     * Get Eye4fraud's custom "insured" status label
     *
     * @return string
     */
    public function getEye4fraudInsuredStatusLabel()
    {
        return 'Insured (Eye4fraud)';
    }

    /**
     * Get Eye4fraud's custom "approved" status code
     *
     * @return string
     */
    public function getEye4fraudApprovedStatusCode()
    {
        return 'eye4fraud_approved';
    }


    /**
     * Get the current create Order state from the configuration
     *
     * @return string
     */
    public function getSelectedCreateOrderState()
    {
        $state = Mage::helper('eye4fraud_connector')->getCreateOrderState();

        if (!in_array($state, array(Mage_Sales_Model_Order::STATE_PROCESSING, Mage_Sales_Model_Order::STATE_HOLDED))) {
            $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        }

        return $state;
    }

    /**
     * Get the current Create Order status from the configuration
     *
     * @return string
     */

    public function getSelectedCreateOrderStatus()
    {
        $status = Mage::helper('eye4fraud_connector')->getCreateOrderStatus();

        $allowedStatuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->getSelectedCreateOrderState());
        if (!in_array($status, array_keys($allowedStatuses))) {
            $status = $this->getEye4fraudApprovedStatusCode();
            Mage::helper('eye4fraud_connector/log')->log("Create Order status: " . $status . " not found in: " . var_export($allowedStatuses,
                    1));
        }

        return $status;
    }


    /**
     * Get Eye4fraud's custom "approved" status label
     *
     * @return string
     */
    public function getEye4fraudApprovedStatusLabel()
    {
        return 'Approved (Eye4fraud)';
    }

    /**
     * Get the current approved state from the configuration
     *
     * @return string
     */
    public function getSelectedApprovedState()
    {
        $state = Mage::helper('eye4fraud_connector')->getApprovedState();

        if (!in_array($state, array(Mage_Sales_Model_Order::STATE_PROCESSING, Mage_Sales_Model_Order::STATE_HOLDED))) {
            $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        }

        return $state;
    }

    /**
     * Get the current approved status from the configuration
     *
     * @return string
     */
    public function getSelectedApprovedStatus()
    {
        $status = Mage::helper('eye4fraud_connector')->getApprovedStatus();

        $allowedStatuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->getSelectedApprovedState());
        if (!in_array($status, array_keys($allowedStatuses))) {
            $status = $this->getEye4fraudApprovedStatusCode();
            Mage::helper('eye4fraud_connector/log')->log("approved status: " . $status . " not found in: " . var_export($allowedStatuses,
                    1));
        }

        return $status;
    }

    /**
     * Get the current declined status from the configuration
     *
     * @return string
     */
    public function getSelectedDeclinedState()
    {
        $state = Mage::helper('eye4fraud_connector')->getDeclinedState();

        if (!in_array($state, array(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_HOLDED))) {
            $state = Mage_Sales_Model_Order::STATE_CANCELED;
        }

        return $state;
    }

    /**
     * Get the current declined status from the configuration
     *
     * @return string
     */
    public function getSelectedDeclinedStatus()
    {
        $status = Mage::helper('eye4fraud_connector')->getDeclinedStatus();

        $allowedStatuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->getSelectedDeclinedState());
        if (!in_array($status, array_keys($allowedStatuses))) {
            $status = $this->getEye4fraudDeclinedStatusCode();
            Mage::helper('eye4fraud_connector/log')->log("declined status: " . $status . " not found in: " . var_export($allowedStatuses,
                    1));
        }

        return $status;
    }

    /**
     * Get the current insured state from the configuration
     *
     * @return string
     */
    public function getSelectedInsuredState()
    {
        $state = Mage::helper('eye4fraud_connector')->getInsuredState();

        if (!in_array($state, array(Mage_Sales_Model_Order::STATE_PROCESSING, Mage_Sales_Model_Order::STATE_HOLDED))) {
            $state = Mage_Sales_Model_Order::STATE_PROCESSING;
        }

        return $state;
    }

    /**
     * Get the current insured status from the configuration
     *
     * @return string
     */
    public function getSelectedInsuredStatus()
    {
        $status = Mage::helper('eye4fraud_connector')->getInsuredStatus();

        $allowedStatuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->getSelectedInsuredState());
        if (!in_array($status, array_keys($allowedStatuses))) {
            $status = $this->getEye4fraudInsuredStatusCode();
            Mage::helper('eye4fraud_connector/log')->log("Insured status: " . $status . " not found in: " . var_export($allowedStatuses,
                    1));
        }

        return $status;
    }

    /**
     * Get the current declined status from the configuration
     *
     * @return string
     */
    public function getSelectedFraudState()
    {
        $state = Mage::helper('eye4fraud_connector')->getFraudState();

        if (!in_array($state, array(Mage_Sales_Model_Order::STATE_CANCELED, Mage_Sales_Model_Order::STATE_HOLDED))) {
            $state = Mage_Sales_Model_Order::STATE_CANCELED;
        }

        return $state;
    }

    /**
     * Get the current fraud status from the configuration
     *
     * @return string
     */
    public function getSelectedFraudStatus()
    {
        $status = Mage::helper('eye4fraud_connector')->getFraudStatus();

        $allowedStatuses = Mage::getSingleton('sales/order_config')->getStateStatuses($this->getSelectedFraudState());
        if (!in_array($status, array_keys($allowedStatuses))) {
            $status = $this->getEye4fraudFraudStatusCode();
            Mage::helper('eye4fraud_connector/log')->log("fraud status: " . $status . " not found in: " . var_export($allowedStatuses,
                    1));
        }

        return $status;
    }
}