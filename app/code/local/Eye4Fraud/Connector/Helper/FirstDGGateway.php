<?php

class Eye4Fraud_Connector_Helper_FirstDGGateway extends Mage_Core_Helper_Abstract
{
    /**
     * Translate AE codes to global
     * @param $code
     * @return mixed
     */
    public function translateAVSCode($code)
    {
        /*
        Y - Exact match, 5 digit zip - Street Address, and 5 digit ZIP Code match
        A - Partial match - Street Address matches, ZIP Code does not
        W - Partial match - ZIP Code matches, Street Address does not
        Z - Partial match - 5 digit ZIP Code match only
        N - No match - No Address or ZIP Code match
        U - Unavailable - Address information is unavailable for that account number, or the card issuer does not support
        G - Service Not supported, non-US Issuer does not participate
        R - Retry - Issuer system unavailable, retry later
        E - Not a mail or phone order
        S - Service not supported
        Q - Bill to address did not pass edit checks/Card Association can't verify the authentication of an address
        D - International street address and postal code match
        B - International street address match, postal code not verified due to incompatible formats
        C - International street address and postal code not verified due to incompatible formats
        P - International postal code match, street address not verified due to incompatible format
        1 - Cardholder name matches
        2 - Cardholder name, billing address, and postal code match
        3 - Cardholder name and billing postal code match
        4 - Cardholder name and billing address match
        5 - Cardholder name incorrect, billing address and postal code match
        6 - Cardholder name incorrect, billing postal code matches
        7 - Cardholder name incorrect, billing address matches
        8 - Cardholder name, billing address, and postal code are all incorrect
        */
        // Allowed  Y, Z, A, X, G, S.
        $codes = array(
            '2' => 'Y',
            '3' => 'Z',
            '4' => 'A',
            '5' => 'Y',
            '6' => 'Z',
            '7' => 'A',
            '8' => 'N'
        );

        if(isset($codes[$code])) return $codes[$code];

        return $code;
    }
}