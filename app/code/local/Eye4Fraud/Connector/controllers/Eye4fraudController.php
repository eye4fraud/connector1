<?php

/**
 * Eye4fraud admin area controller
 *
 * @category   Eye4Fraud
 * @package    Eye4Fraud_Connector
 * @author     Michael
 */
class Eye4Fraud_Connector_Eye4fraudController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('system/config/eye4fraud_connector_logfile');
    }

    /**
     * Customer addresses list
     */
    public function indexAction()
    {
        return $this->_redirect("*/*/index");
    }

    /**
     * Remove saved card
     */
    public function logfileAction()
    {

		$helper = Mage::helper("eye4fraud_connector");
		if(!$helper->getLogSize()) return;

		$response = $this->getResponse();
        $response->clearAllHeaders();

		$idx = $this->getRequest()->getParam('idx');
		if(!is_null($idx)){
            $response->setHeader('Content-type', 'application/gzip');
            $response->setHeader('Content-encoding', 'gzip');
			//header("Content-type: application/gzip");
            $response->setHeader('Content-Disposition', 'attachment; filename=eye4fraud_debug'.$idx.'.log.gz');
			//header("Content-Disposition: attachment; filename=eye4fraud_debug".$idx.'.log.gz');
			$file_path = $helper->getLogFilePath().$idx.'.gz';
		}
		else{
            $response->setHeader('Content-type', 'text/plain');
			//header("Content-type: text/plain");
            $response->setHeader('Content-Disposition', 'attachment; filename=eye4fraud_debug.log');
			//header("Content-Disposition: attachment; filename=eye4fraud_debug.log");
			$file_path = $helper->getLogFilePath();
		}

		if(!file_exists($file_path)) return;

		$f = fopen($file_path, 'r');
        $data = "";
		while (!feof($f)) {
            $data .= fgets($f);
		}

        fclose($f);
        return $response->setBody($data);
    }

    /**
     * Refresh eye4fraud status for orders
     */
    public function refreshAction(){
        $orderIds = $this->getRequest()->getPost('order_ids', array());
        $ordersCollection = Mage::getResourceModel('sales/order_collection');
        $ordersCollection->addFieldToFilter('entity_id', array('in' => $orderIds));
        $ordersCollection->addFieldToSelect('increment_id');
        $select = $ordersCollection->getSelect();
        $data = $ordersCollection->getConnection()->fetchAll($select);
        $ids = array_map('array_pop', $data);

        $helper = Mage::helper('eye4fraud_connector');

        $helper->log("Force refresh statuses for : ".json_encode($ids));
        $statusesCollection = Mage::getResourceSingleton('eye4fraud_connector/status_collection');
        $statusesCollection->setOrdersFilter($ids);
        $statusesCollection->setCronFlag(true);
        $records_count = $statusesCollection->count();

        if(!$helper->getConfigStatusControlActive() && $helper->getConfigCancelOrder()) foreach($statusesCollection as $status){
            $helper->cancelOrder($status);
        };

        if (!$records_count) {
            $this->_getSession()->addError($this->__('Problem with status refreshing'));
        } else {
            $this->_getSession()->addSuccess($this->__('Eye4Fraud Status was refreshed for requested statuses'));
        }

        $this->_redirect('adminhtml/sales_order/index');
    }

    /**
     * Refresh eye4fraud status for orders
     */
    public function resendAction()
    {
        $orderIds = $this->getRequest()->getPost('order_ids', array());
        /** @var Eye4Fraud_Connector_Model_Observer $observer */
        $observer = Mage::getSingleton('eye4fraud_connector/observer');
        $helper = Mage::helper('eye4fraud_connector');
        $helper->log("Force resend orders: ".json_encode($orderIds));

        /** @var Mage_Sales_Model_Resource_Order_Collection $ordersCollection */
        $ordersCollection = Mage::getResourceModel('sales/order_collection');
        $ordersCollection->addFieldToFilter('entity_id', array('in' => $orderIds));

        $processed = 0;
        foreach ($ordersCollection as $order) {
            /** @var Mage_Sales_Model_Order $order */
            if ($observer->processOrder($order)) {
                $processed ++;
            };
        }

        if (!$processed) {
            $this->_getSession()->addError($this->__('No orders will be resend'));
        } else {
            $this->_getSession()->addSuccess($this->__('%s order(s) was queued to resend', $processed));
        }

        $this->_redirect('adminhtml/sales_order/index');
    }
}