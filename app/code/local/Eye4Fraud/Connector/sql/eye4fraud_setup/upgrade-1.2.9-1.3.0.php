<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$helper = Mage::helper('eye4fraud_connector/order_status');

// create new approved and declined statuses
$helper = Mage::helper('eye4fraud_connector/order_status');

Mage::getModel('sales/order_status')
	->setStatus($helper->getTransportErrorStatusCode())->setLabel($helper->getTransportErrorStatusLabel())
	->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
	->save();

Mage::getModel('sales/order_status')
	->setStatus($helper->getEye4fraudDeclinedStatusCode())
    ->setLabel($helper->getEye4fraudDeclinedStatusLabel())
	->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
	->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudDeclinedStatusCode())
    ->setLabel($helper->getEye4fraudDeclinedStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_CANCELED)
    ->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudApprovedStatusCode())
    ->setLabel($helper->getEye4fraudApprovedStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
    ->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudApprovedStatusCode())
    ->setLabel($helper->getEye4fraudApprovedStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_PROCESSING)
    ->save();

//  new eye4fraud   status fraud and insured

Mage::getModel('sales/order_status')
	->setStatus($helper->getEye4fraudFraudStatusCode())
    ->setLabel($helper->getEye4fraudFraudStatusLabel())
	->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
	->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudFraudStatusCode())
    ->setLabel($helper->getEye4fraudFraudStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_CANCELED)
    ->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudInsuredStatusCode())
    ->setLabel($helper->getEye4fraudInsuredStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
    ->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getEye4fraudInsuredStatusCode())
    ->setLabel($helper->getEye4fraudInsuredStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_PROCESSING)
    ->save();

// update or create existing on hold for review and transport error labels and statuses
Mage::getModel('sales/order_status')
    ->setStatus($helper->getOnHoldStatusCode())
    ->setLabel($helper->getOnHoldStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
    ->save();

// Status for payment methods to hold order after payment and prevent messing with other extensions
Mage::getModel('sales/order_status')
    ->setStatus($helper->getOnHoldStatusCode())
    ->setLabel($helper->getOnHoldStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_PROCESSING)
    ->save();

Mage::getModel('sales/order_status')
    ->setStatus($helper->getTransportErrorStatusCode())
    ->setLabel($helper->getTransportErrorStatusLabel())
    ->assignState(Mage_Sales_Model_Order::STATE_HOLDED)
    ->save();


$installer->endSetup();
