<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create 'Eye4Fraud_Connector/saved_cards' table
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('eye4fraud_connector/saved_cards'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'ID field')
    ->addColumn('payment_method_code', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
        'nullable'  => false,
    ), 'Payment Method Code')
    ->addColumn('card_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'Saved card ID for payment method')
    ->addColumn('first6', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(
        'nullable'  => false,
    ), 'First 6 digits')
    ->addColumn('last4', Varien_Db_Ddl_Table::TYPE_VARCHAR, 5, array(
        'nullable'  => false,
    ), 'Last 4 digits')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
        'nullable'  => false,
    ), 'Created Time')
    ->addIndex(
        $installer->getIdxName('eye4fraud_connector/saved_cards', array('payment_method_code','card_id')),
        array('payment_method_code','card_id')
    )
    ->setComment('Keep cards data for saved cards');

$installer->getConnection()->createTable($table);

$installer->endSetup();
