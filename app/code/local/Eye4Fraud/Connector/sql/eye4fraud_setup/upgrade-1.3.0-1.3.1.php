<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()->addColumn(
    $installer->getTable('eye4fraud_connector/requests_cache'),
    'hold',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '0',
        'comment'   => '1 to temporary prevent request sending'
    )
);

$installer->endSetup();
