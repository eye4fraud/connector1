<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Update fields 'Eye4Fraud_Connector/saved_cards' table
 */
$table = $installer->getConnection()->changeColumn(
    $installer->getTable('eye4fraud_connector/saved_cards'),
    'payment_method_code',
    'payment_method_code',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length' => 70,
        'comment'   => 'Request ID'
    )
);
$table = $installer->getConnection()->changeColumn(
    $installer->getTable('eye4fraud_connector/saved_cards'),
    'card_id',
    'card_id',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => false,
        'length' => 60,
        'comment'   => 'Request ID'
    )
);

$installer->endSetup();
