<?php
/**
 * Model of single fraud status for one order
 *
 * @category   Eye4Fraud
 * @package    Eye4fraud_Connector
 */
class Eye4Fraud_Connector_Model_Cards extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'eye4fraud_connector_cards';

    protected function _construct(){
        $this->_init('eye4fraud_connector/cards');
    }

}