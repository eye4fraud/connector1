<?php

/**
 * Rewrite original class, set own event prefix
 */
class Eye4Fraud_Connector_Model_Authnetcim_Card extends ParadoxLabs_AuthorizeNetCim_Model_Card {

    protected $_eventPrefix = 'authnetcim_card';

}
