<?php

/**
 * Extend Rootways Elavon payment instance to keep access to response data

 * @category    Eye4Fraud
 * @package     Eye4Fraud_Connector
 * @author      Mikhail Valiushko
 */
class Eye4Fraud_Connector_Model_Rootways_Elavon_Payment extends Rootways_Elavon_Model_Elavon
{
    public function authorize(Varien_Object $payment, $amount)
    {
        $config = Mage::getConfig();
        $module_config = $config->getModuleConfig('Rootways_Elavon');
        $version = (string)$module_config->version;
        if ($version === null || version_compare($version, '1.0.0', '>')) {
            return parent::authorize($payment, $amount);
        }

        if($amount<=0) {
            $errorMsg = $this->_getHelper()->__('Invalid amount for capture.');
        }
        $ref_id = 0;
        $trn_type = 'a';
        $order = $payment->getOrder();
        $result = $this->createCharge($payment,$amount,$trn_type,$ref_id);

        if($result->ssl_result != '0') {
            $errorMessage = $result->errorMessage;
            $errorMsg = $this->_getHelper()->__('Error Processing the request. '.$errorMessage);
        } else {
            if($result->ssl_result == '0'){
                $payment->setTransactionId($result->ssl_txn_id);
                $payment->setIsTransactionClosed(0);
                $payment->setCcCidStatus((string)$result->ssl_cvv2_response);
                $payment->setCcAvsStatus((string)$result->ssl_avs_response);
                $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,array('key1'=>'value1','key2'=>'value2'));
            }else{
                Mage::throwException($errorMsg);
            }
        }
        if($errorMsg){
            Mage::throwException($errorMsg);
        }
        return $this;
    }

    /**
     * Class for capture payment transaction.
     */
    public function capture(Varien_Object $payment, $amount)
    {
        $config = Mage::getConfig();
        $module_config = $config->getModuleConfig('Rootways_Elavon');
        $version = (string)$module_config->version;
        if ($version === null || version_compare($version, '1.0.0', '>')) {
            return parent::capture($payment, $amount);
        }

        if($amount<=0) {
            $errorMsg = $this->_getHelper()->__('Invalid amount for capture.');
        }

        $order = $payment->getOrder();
        if($payment->getTransactionId() != ''){
            $ref_id = $payment->getTransactionId();
        } else {
            $ref_id = 0;
        }
        $trn_type = 'c';
        $result = $this->createCharge($payment,$amount,$trn_type,$ref_id);

        if($result->ssl_result != '0') {
            $errorMessage = $result->errorMessage;
            $errorMsg = $this->_getHelper()->__('Error Processing the request. '.$errorMessage);
        } else {
            if($result->ssl_result == '0'){
                $payment->setTransactionId($result->ssl_txn_id);
                $payment->setIsTransactionClosed(1);
                $payment->setCcCidStatus((string)$result->ssl_cvv2_response);
                $payment->setCcAvsStatus((string)$result->ssl_avs_response);
                $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,array('key1'=>'value1','key2'=>'value2'));
            }else{
                Mage::throwException($errorMsg);
            }
        }
        if($errorMsg){
            Mage::throwException($errorMsg);
        }
        return $this;
    }

    /**
     * Class for create charge.
     */
    protected function createCharge(Varien_Object $payment, $amount, $trn_type, $ref_id)
    {
        $order = $payment->getOrder();

        $ssl_merchant_id = $this->_getHelper()->getMerchantId();
        $ssl_user_id = $this->_getHelper()->getUserId();
        $ssl_pin = $this->_getHelper()->getElavonPin();

        $ssl_test_mode = $this->_getHelper()->getPaymentMode();
        $ssl_amount = number_format($amount, 2, '.', '');
        $ssl_transaction_currency = Mage::app()->getStore()->getCurrentCurrencyCode();
        $ssl_description =  $this->_getHelper()->__('Order # %s', $order->getIncrementId());

        // *** Credit Card information ***
        $ssl_card_number = $payment->getCcNumber();
        $ssl_exp_date = DateTime::createFromFormat('m', $payment->getCcExpMonth())->format('m').DateTime::createFromFormat('Y', $payment->getCcExpYear())->format('y'); //"0817";
        $ssl_cvv2cvc2 = $payment->getCcCid();
        $ssl_cvv2cvc2_indicator = '1';
        if($trn_type == 'a'){
            $ssl_transaction_type = 'ccauthonly';
        } elseif($trn_type == 'c' && $ref_id != '0'){
            $ssl_transaction_type = 'cccomplete';
        } else if ($trn_type == 'r'){
            $ssl_transaction_type = 'ccreturn';
        } else {
            $ssl_transaction_type = 'ccsale';
        }

        //  *** Credit Card Billing information ***
        $billingaddress = $order->getBillingAddress();
        $ssl_first_name = $billingaddress->getData('firstname');
        $ssl_last_name= $billingaddress->getData('lastname');
        $ssl_company = $billingaddress->getData('company');
        $ssl_avs_address = $billingaddress->getData('street');
        $ssl_city = $billingaddress->getData('city');
        $ssl_state = Mage::getModel('directory/region')->load($billingaddress->getData('region_id'))->getCode();
        $ssl_country = Mage::getModel('directory/country')->loadByCode($billingaddress->getData('country_id'))->getName();
        $ssl_avs_zip = $billingaddress->getData('postcode');
        $ssl_phone = $billingaddress->getData('telephone');
        $ssl_email = $billingaddress->getData('email');

        if (empty($billingaddress)) {
            $errorMsg = $this->_getHelper()->__('Invalid billing data.');
        }
        if($errorMsg){
            Mage::throwException($errorMsg);
        }

        if($trn_type != 'a' && $ref_id != '0'){
            $charge_id = str_replace('-capture','',$ref_id);
            $xml = "<txn><ssl_merchant_id>" . $ssl_merchant_id . "</ssl_merchant_id>
                    <ssl_pin>" . $ssl_pin . "</ssl_pin>
                    <ssl_user_id>" . $ssl_user_id . "</ssl_user_id>
                    <ssl_transaction_type>".$ssl_transaction_type."</ssl_transaction_type>
                    <ssl_description>".$ssl_description."</ssl_description>
                    <ssl_txn_id>".$charge_id."</ssl_txn_id>
                </txn>";
        } else {
            $xml = "<txn>";
            $xml .= "
                <ssl_merchant_id>" . $ssl_merchant_id . "</ssl_merchant_id>
                <ssl_pin>" . $ssl_pin . "</ssl_pin>
                <ssl_user_id>" . $ssl_user_id . "</ssl_user_id>
                <ssl_test_mode>" . $ssl_test_mode . "</ssl_test_mode>
                <ssl_transaction_type>" . $ssl_transaction_type . "</ssl_transaction_type>
                <ssl_card_number>" . $ssl_card_number . "</ssl_card_number>
                <ssl_exp_date>" . $ssl_exp_date . "</ssl_exp_date>
                <ssl_amount>" . $ssl_amount . "</ssl_amount>
                <ssl_invoice_number>" . $order->getIncrementId() . "</ssl_invoice_number>
                <ssl_description>" . $ssl_description . "</ssl_description>
                <products>" . $ssl_amount . "::1::001::" . $ssl_description . "::</products>
                <ssl_cvv2cvc2_indicator>" . $ssl_cvv2cvc2_indicator . "</ssl_cvv2cvc2_indicator>
                <ssl_cvv2cvc2>" . $ssl_cvv2cvc2 . "</ssl_cvv2cvc2>
                <ssl_first_name>" . $ssl_first_name . "</ssl_first_name>
                <ssl_last_name>" . $ssl_last_name . "</ssl_last_name>
                <ssl_company>" . $ssl_company . "</ssl_company>
                <ssl_avs_address>" . substr($ssl_avs_address,0,25) . "</ssl_avs_address>
                <ssl_city>" . $ssl_city . "</ssl_city>
                <ssl_state>" . $ssl_state . "</ssl_state>
                <ssl_country>" . $ssl_country . "</ssl_country>
                <ssl_avs_zip>" . $ssl_avs_zip . "</ssl_avs_zip>
                <ssl_phone>" . $ssl_phone . "</ssl_phone>
                <ssl_email>" . $ssl_email . "</ssl_email>";
            if ( Mage::getStoreConfig('payment/rootwayselavon_option/multi_currency') == 1 ) {
                $xml .= "<ssl_transaction_currency>" . $ssl_transaction_currency . "</ssl_transaction_currency>";
            }
            $xml .= '</txn>';
        }
        $postURL = $this->_getHelper()->getPostUrl();
        $postData = "xmldata=" . URLEncode($xml);

        $session = curl_init();
        $header[] = "Content-Length: " . strlen($postData);
        $header[] = "Content-Type: application/x-www-form-urlencoded";

        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($session, CURLOPT_URL, $postURL);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_HTTPHEADER, $header);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($session);
        $result = simplexml_load_string($response);
        return $result;
    }
}