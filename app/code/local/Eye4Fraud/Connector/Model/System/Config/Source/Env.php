<?php

class Eye4Fraud_Connector_Model_System_Config_Source_Env
{     
    public function toOptionArray()
    {
        return array(
            array('value' => 'PROD', 'label' => Mage::helper('eye4fraud_connector')->__('Production')),
            array('value' => 'SANDBOX', 'label' => Mage::helper('eye4fraud_connector')->__('Sandbox')),
            array('value' => 'DEV', 'label' => Mage::helper('eye4fraud_connector')->__('Dev'))
        );
    }
}