<?php

class Eye4Fraud_Connector_Model_System_Config_Source_FraudState
{
	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		return array(
			array('value' => Mage_Sales_Model_Order::STATE_CANCELED, 'label' => Mage::helper('eye4fraud_connector')->__(Mage_Sales_Model_Order::STATE_CANCELED)),
			array('value' => Mage_Sales_Model_Order::STATE_HOLDED, 'label' => Mage::helper('eye4fraud_connector')->__(Mage_Sales_Model_Order::STATE_HOLDED))
		);
	}
}