<?php

class Eye4Fraud_Connector_Model_System_Config_Source_HoldedStateStatuses
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $arr = Mage::getSingleton('sales/order_config')->getStateStatuses(Mage_Sales_Model_Order::STATE_HOLDED);
        return array_map(function($status_code,$status_label) {
            return array('value' => $status_code, 'label' => Mage::helper('eye4fraud_connector')->__($status_label));
        }, array_keys($arr),$arr);
    }
}