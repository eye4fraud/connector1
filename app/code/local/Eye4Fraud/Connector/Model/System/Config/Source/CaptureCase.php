<?php

class Eye4Fraud_Connector_Model_System_Config_Source_CaptureCase
{
	/**
	 * @return array
	 */
	public function toOptionArray()
	{
		return array(
			array('value' => Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE, 'label' => Mage::helper('eye4fraud_connector')->__('Capture Online')),
			array('value' => Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE, 'label' => Mage::helper('eye4fraud_connector')->__('Capture Offline'))
		);
	}
}