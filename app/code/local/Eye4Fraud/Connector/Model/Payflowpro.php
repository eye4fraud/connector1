<?php

/**
 * Payflow Pro payment gateway model
 *
 * @category    Eye4Fraud
 * @package     Eye4Fraud_Connector
 * @author      Mikhail Valiushko
 */

class Eye4Fraud_Connector_Model_Payflowpro extends  Mage_Paypal_Model_Payflowpro
{
    protected $_responseData = array();

    /**
     * Post request to gateway and return response
     *
     * @param Varien_Object $request
     * @return Varien_Object
     * @throws Exception
     */
    protected function _postRequest(Varien_Object $request)
    {
        $this->_responseData = parent::_postRequest($request);

        return $this->_responseData;
    }

    /**
     * Return response data
     * @return array
     */
    public function getResponseData(){
        return $this->_responseData;
    }

}