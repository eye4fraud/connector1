<?php

/**
 * Model of single fraud status for one order
 *
 * @category   Eye4Fraud
 * @package    Eye4fraud_Connector
 */
class Eye4Fraud_Connector_Model_Status extends Mage_Core_Model_Abstract
{
    protected $_eventPrefix = 'eye4fraud_connector_status';

    /**
     * Order ID
     * @var int
     */
    protected $_orderIdSaved = 0;

    /**
     * New order status to update orders grid collection later
     * @var string
     */
    protected $_orderStatusNew = '';

    /**
     * Set order matched to this status
     * @var Mage_Sales_Model_Order
     */
    protected $_order = null;

    protected function _construct()
    {
        $this->_init('eye4fraud_connector/status');
    }

    /**
     * Retrieve status from remote server and save model
     * @return $this
     * @throws Exception
     */
    public function retrieveStatus()
    {
        $helper = Mage::helper('eye4fraud_connector');
        $fraudData = $helper->getOrderStatus($this->getData('order_id'));
        if (empty($fraudData)) {
            $this->setData('status', 'RER');
            $this->setData('description', 'Connection Error');
            $this->setData('updated_at', Mage::getSingleton('core/date')->date('Y-m-d H:i:s'));
            return $this;
        }

        if (isset($fraudData['error']) and $fraudData['error']) {
            $this->setData('error', true);
        }

        $status = $fraudData['StatusCode'];
        if ($fraudData['StatusCode'] == 'E' and strpos($fraudData['Description'], 'No Order') !== false) {
            $status = 'N';
        }

        $this->autoSync($fraudData, $status);

        // End Change order status if Enable
        $this->setData('status', $status);
        $this->setData('description', $fraudData['Description']);
        $this->setData('updated_at', Mage::getSingleton('core/date')->date('Y-m-d H:i:s'));
        if (!$this->getOrigData('created_at')) {
            $this->setData('created_at', Mage::getSingleton('core/date')->date('Y-m-d H:i:s'));
        }

        /**
         * A little hack to restore order_id field after model was saved
         */
        $this->_orderIdSaved = $this->getData('order_id');
        $this->save();
        $this->setData('order_id', $this->_orderIdSaved);
        return $this;
    }

    /**
     * Autosync order
     * @param array $fraudData
     * @param string $status
     * @return $this
     */
    protected function autoSync($fraudData, $status)
    {
        $helper = Mage::helper('eye4fraud_connector');
        // Change order status if Enable
        $autoSync = Mage::helper('eye4fraud_connector');
        /** @var Eye4Fraud_Connector_Helper_Order_Status $eyeStatus */
        $eyeStatus = Mage::helper('eye4fraud_connector/order_status');
        $eyefraudHelper = Mage::helper('eye4fraud_connector/order_invoice');
        $newState = '';
        $newStatus = '';
        $description = $fraudData['Description'];
        if ($autoSync->getConfigStatusControlActive()) {
            if ($status == "D" or $status == "A" or $status == "I" or $status == "F") {
                /** @var Mage_Sales_Model_Order $orderLoad */
                $orderLoad = Mage::getModel('sales/order')->load($this->getData('order_id'), 'increment_id');

                $payment = $orderLoad->getPayment();
                /** @var string $paymentMethod */
                $paymentMethod = $payment->getData("method");
                if ($autoSync->getConfigIgnorePPEOrders() and in_array($paymentMethod, array(
                        Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS,
                        Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS,
                    ))
                ) {
                    $helper->log('Payment method is ' . $paymentMethod . ' order control is ignored');
                    return $this;
                }

                $currentState = $orderLoad->getState();
                $currentStatus = $orderLoad->getStatus();

                $eventData = array(
                    'order' => $orderLoad,
                    'status' => $status,
                    'old_status' => $orderLoad->getStatus(),
                    'description' => $description
                );

                switch ($status) {
                    case 'A':{
                        //$eyeStatus = "approved";
                        $newState = $eyeStatus->getSelectedApprovedState();
                        $newStatus = $eyeStatus->getSelectedApprovedStatus();
                        break;
                    }
                    case 'D':{
                        //  $eyeStatus = "declined";
                        $newState = $eyeStatus->getSelectedDeclinedState();
                        $newStatus = $eyeStatus->getSelectedDeclinedStatus();
                        break;
                    }
                    case 'I':{
                        //  $eyeStatus = "insured";
                        $newState = $eyeStatus->getSelectedInsuredState();
                        $newStatus = $eyeStatus->getSelectedInsuredStatus();
                        break;
                    }
                    case 'F':{
                        //  $eyeStatus = "fraud";
                        $newState = $eyeStatus->getSelectedFraudState();
                        $newStatus = $eyeStatus->getSelectedFraudStatus();
                        break;
                    }
                }

                $changed = false;
                // if newState exists and new state/status are different from current and config is set to status-sync
                if ($newState && ($newState != $currentState || $newStatus != $currentStatus) && $autoSync->getConfigStatusControlActive()) {
                    if ($orderLoad->getState() === Mage_Sales_Model_Order::STATE_HOLDED) {
                        //$orderLoad->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
                        try {
                            $orderLoad->unhold();
                        } catch (Exception $e) {
                            $autoSync->log("Error while unholding order: '" . $e->getMessage());
                        }
                    }

                    if ($newState == Mage_Sales_Model_Order::STATE_CANCELED) {
                        $autoSync->log("Order '" . $orderLoad->getId() . "' should be canceled - calling cancel method");
                        try {
                            $payment = $orderLoad->getPayment();
                            if ($payment->getMethod() === Mage_Paypal_Model_Config::METHOD_WPS and $eyefraudHelper->getCaptureCase() === Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE) {
                                $paypal_standard_instance = $payment->getMethodInstance();
                                if ($paypal_standard_instance->getConfigPaymentAction() === 'Authorization') {
                                    $info = $paypal_standard_instance->getInfoInstance();
                                    $instance = Mage::helper('payment')->getMethodInstance(Mage_Paypal_Model_Config::METHOD_WPP_DIRECT);
                                    if ($instance) {
                                        $instance->setInfoInstance($info);
                                        $info->setMethodInstance($instance);
                                        $payment->setData('method_instance', $instance);
                                    }
                                }
                            }

                            $orderLoad->cancel();
                        } catch (Exception $e) {
                            $autoSync->log("Error while canceling order: '" . $e->getMessage());
                        }
                    }

                    $orderLoad->setData('state', $newState);
                    $orderLoad->setStatus($newStatus);
                    $history = $orderLoad->addStatusHistoryComment('Order status changed by Eye4Fraud', false);
                    $history->setIsCustomerNotified(false);
                    // $orderLoad->setState($newState, $newStatus, $description);
                    $autoSync->log("Updated order '" . $orderLoad->getId() . "' to: state:  '$newState', status: '$newStatus', description: '$description'");
                    $changed = true;
                }
                /* elseif ($description && $riskifiedStatus != $riskifiedOldStatus) {
                    $autoSync->log("Updated order " . $orderLoad->getId() . " history comment to: " . $description);
                    $orderLoad->addStatusHistoryComment($description);
                    $changed = true;
                } */ else {
                    $autoSync->log("No update to state,status,comments is required for " . $orderLoad->getId());
                }


                if ($changed) {
                    try {
                        $orderLoad->save();
                        if ($status == 'A' || $status === 'I') {
                            Mage::dispatchEvent(
                                'eyefraud_full_order_update_approved', $eventData
                            );
                        }

                        /**
                         * @todo: order status isn't right for Approved orders,r require debug
                         */
                        //$this->order_status_new = $orderLoad->getStatus();

                    } catch (Exception $e) {
                        $autoSync->log("Error saving order: " . $e->getMessage());
                        return $this;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Create queued status after request was cached
     * @param string $orderId
     * @return $this
     */
    public function createQueued($orderId)
    {
        $this->setData('order_id', $orderId);
        $this->setData('status', 'Q');
        $this->setData('description', 'Request Queued');
        $this->setData('updated_at', Mage::getSingleton('core/date')->gmtDate());
        $this->setData('created_at', Mage::getSingleton('core/date')->gmtDate());
        $this->isObjectNew(true);
        return $this;
    }

    /**
     * Changes status to Awaiting Response and allow to save
     */
    public function setWaitingStatus()
    {
        $this->setData('status', 'W');
        $this->setData('description', 'Waiting Update');
        return $this;
    }

    /**
     * Set or get flag is object new
     * @param null $flag
     * @return bool
     */
    public function isObjectNew($flag = null)
    {
        $this->getResource()->setNewFlag(true);
        return parent::isObjectNew($flag);
    }

    /**
     * Update fraud status in order and in grid after fraud status was changed
     * @return Mage_Core_Model_Abstract
     */
    public function _afterSave()
    {
        $requireSave = false;
        if (!$this->_order) {
            if ($this->_orderIdSaved) {
                $this->_order = Mage::getModel('eye4fraud_connector/sales_order')->loadByIncrementId($this->_orderIdSaved);
            } else {
                $this->_order = Mage::getModel('eye4fraud_connector/sales_order')->loadByIncrementId($this->getData('order_id'));
            }

            $requireSave = true;
        }

        $helper = Mage::helper('eye4fraud_connector');
        $statusText = $helper->__('status:' . $this->getData('status'));

        if (!$this->_order->isEmpty() and $this->_order->getData('eye4fraud_status') != $statusText) {
            $this->_order->setData('eye4fraud_status', $statusText);
            $helper->log('Save fraud status ' . $statusText . ' to order #' . $this->_order->getIncrementId());
            if ($requireSave) {
                try{
                    $this->_order->save();
                }
                catch (Exception $e){
                    $helper->log('Error while trying to save order');
                    $helper->log($e->getMessage());
                }
            }
        }

        return parent::_afterSave();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function setOrder($order)
    {
        $this->_order = $order;
        return $this;
    }

    /**
     * Require new order status if order was changed by orders management
     * @return string
     */
    public function getNewOrderStatus()
    {
        return $this->_orderStatusNew;
    }
}
