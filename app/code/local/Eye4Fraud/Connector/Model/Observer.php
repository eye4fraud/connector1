<?php
/**
 * Eye4fraud Connector Magento Module
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer
    extends Mage_Core_Model_Mysql4_Abstract
{
    protected $_helper = null;

    /**
     * Cache fraud statuses for orders grid
     * @var array
     */
    protected $ordersStatuses = array();

    protected $card_save_method_code = 'ccsave';

    /**
     * Magento class constructor
     * @return void
     */
    protected function _construct()
    {
        // Method is required
        $z = 1;
    }

    /**
     * Checks if the soap client is enabled.
     * Shows an error message in the admin panel if it's not.
     * @return [type] [description]
     */
    public function checkSoapClient()
    {
        $helper = $this->_getHelper();
        if (!$helper->hasSoapClient()) {
            Mage::getSingleton('core/session')->addError('Your server does not have SoapClient enabled. The EYE4FRAUD extension will not function until the SoapClient is installed/enabled in your server configuration.');
            return false;
        }
        return true;
    }

    /**
     * Order placed after; called from sales_order_place_after event
     * @param $observer
     * @throws Exception
     * @internal param \Varien_Event_Observer $observer
     * @return $this
     */
    public function orderPlacedAfter(&$observer)
    {
		if (!$this->_getHelper()->isEnabled()) return $this;

        $order = $observer->getEvent()->getOrder();
        $payment = $order->getPayment();
        if (empty($payment)) {
            $this->_getHelper()->log('EYE4FRAUD: Invalid payment passed to callback.');
            return $this;
        }
        $this->_processOrder($order, $payment, 'orderPlacedAfter');

        return $this;
    }

    /**
     * Queue order to send to E4F
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function processOrder($order)
    {
        if (!$this->_getHelper()->isEnabled()) return false;

        $payment = $order->getPayment();
        $this->_processOrder($order, $payment, 'processOrder');
        return true;
    }

    /**
     * Function to process order; called from orderPlacedAfter method
     * @param  Mage_Sales_Model_Order $order
     * @param  Mage_Sales_Model_Order_Payment $payment
     * @param  string $methodName
     * @return void
     */
    protected function _processOrder(\Mage_Sales_Model_Order $order, \Mage_Sales_Model_Order_Payment $payment, $methodName)
    {
        /**
         * Someone write this very long text
         * @todo move this to separate model, create separate model for each supported payment method
         * @todo getModel('eye4fraud_connector/payment_{payment_code}')
         */
        try {
            $helper = $this->_getHelper();
            $config = $helper->getConfig('', $order->getStoreId());
            
            $request = Mage::app()->getRequest();

            /**
             * State of created cached request, set 1 to hold for further processing
             */
            $requestState = 0;

            //Make sure we have a config array, and that we're enabled
            if (empty($config) || !$helper->isEnabled()) {
                return;
            }

            $methodInstance = $payment->getMethodInstance();
            $paymentMethod = $methodInstance->getCode();
            $helper->log("Payment method name: ".$paymentMethod);
            $allow_untested = $helper->getConfig('rewrite_status/allow_untested') === '1';

            if (!$allow_untested &&
                !in_array(
                    $paymentMethod,
                    array(
                        $this->card_save_method_code,
                        $helper::PAYMENT_METHOD_USAEPAY,
                        Mage_Paypal_Model_Config::METHOD_PAYFLOWPRO,
                        Mage_Paypal_Model_Config::METHOD_WPP_DIRECT,
                        Mage_Paypal_Model_Config::METHOD_PAYFLOWLINK,
                        Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS,
                        Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS,
                        Mage_Paypal_Model_Config::METHOD_WPS,
                        Mage_Paygate_Model_Authorizenet::METHOD_CODE,
                        'authorizenet_directpost',
                        'sfc_cim_core', // StoreFront Authorize.Net CIM Tokenized Payment Extension
                        'cls_splitpayment', // Modified version of Authorize.Net payment
                        'authnetcim', //ParadoxLabs AuthorizeNetCim Payment Extension
                        'anattadesign_awesomecheckout_idpauthorize',
                        'ccgateway', // Cardconnect Ccgateway
                        'stripe', // Netgains_Stripe
                        'firstdataglobalgateway', // Firstdata Global Gateway E4
                        'gene_braintree_paypal', // Gene Braintree Payments
                        'gene_braintree_creditcard', // Gene Braintree Payments
                        'subscribe_pro', // Subscribepro
                        'sagepaydirectpro', // SagePay
                        'sagepaydirectpro_moto', // SagePay
                        'sagepayform',// SagePay
                        'sagepaynit',// SagePay
                        'sagepaypaypal',// SagePay
                        'sagepayserver',// SagePay
                        'sagepayserver_moto',// SagePay
                        'sagepaytoken',// SagePay
                        'rootwayselavon_option' // Rootways Magento Elavon/Converge
                    )
                )
            )
            {
                $this->_getHelper()->log("Payment method not supported: ".$paymentMethod);
                return;
            }

            $version = Mage::getVersion();

            /** @var \Mage_Customer_Model_Address $billing */
            $billing = $order->getBillingAddress();
            /** @var \Mage_Customer_Model_Address $shipping */
            $shipping = $order->getShippingAddress();
            /** Make empty shipping object if it not exists */
            if ($shipping===false) {
                $shipping = new Varien_Object();
            } else if (!$shipping->getPostcode() && !$shipping->getData('city')) {
                /** Shipping match billing if not specified */
                $shipping = $billing;
            }

            $postPayment = $request->getPost('payment');

            $items = $order->getAllItems();
            $lineItems = array();
            foreach ($items as $i => $item) {
                /** @var \Mage_Sales_Model_Order_Item $item */;
                $lineItems[$i + 1] = array(
                    'ProductName' => $item->getSku(),
                    'ProductDescription' => $item->getName(),
                    'ProductSellingPrice' => round($item->getBaseRowTotal(), 2),
                    'ProductQty' => round($item->getQtyOrdered(), 2),
                    'ProductCostPrice' => round($item->getBasePrice(), 2),
                    // todo convert currency
                );
            }

            $transInfo = $payment->getTransactionAdditionalInfo();

            $additionalInformation = $payment->additional_information;

            //file_put_contents(Mage::getBaseDir("log")."/debug.log",print_r($payment->debug(), true)."\n",FILE_APPEND);
            //file_put_contents(Mage::getBaseDir("log")."/debug.log",print_r($method_instance->debug(), true)."\n",FILE_APPEND);

            $transId = null;
            switch ($paymentMethod) {
                case 'authnetcim':{
                    $ccNumber = $payment->getData('cc_number');
                    if (!$ccNumber) $ccNumber = $additionalInformation['acc_number'];
                    if ($payment->getData('tokenbase_id')) {
                        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
                        $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
                        $collection->addFieldToFilter('payment_method_code', $payment->getData('method'))->addFieldToFilter('card_id', $payment->getData('tokenbase_id'));
                        $collection->load();
                        if ($collection->count()) {
                            $card = $collection->getFirstItem();
                            $ccNumber = $card['first6'].'00000'.$card['last4'];
                        } else{
                            $ccNumber = '00000000'.$ccNumber;
                        }

                    }
                    $cardTypeRaw = $additionalInformation['card_type'];
                    //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
                    switch ($cardTypeRaw) {
                        case 'Visa': {
                            $cardType = 'VI';
                            break;
                            }
                        case 'MasterCard': {
                            $cardType = 'MC';
                            break;
                            }
                        case 'AmericanExpress': {
                            $cardType = 'AE';
                            break;
                            }
                        case 'Discover': {
                            $cardType = 'DI';
                            break;
                            }
                        default: {
                            $cardType = 'other';
                            break;
                            }
                    }
                    $payment->setData('cc_cid_status', $additionalInformation['card_code_response_code']);
                    $payment->setdata('cc_avs_status', $additionalInformation['avs_result_code']);
                    $transId = $additionalInformation['transaction_id'];
                    break;
                }
                case 'ccgateway':{
                    $paymentDataRaw = Mage::app()->getRequest()->getParam('payment');
                    if(isset($paymentDataRaw['first6'])) {
                        if($paymentDataRaw['first6']==='') $paymentDataRaw['first6'] = '000000';
                        $ccNumber = $paymentDataRaw['first6'].'00000'.substr($paymentDataRaw['cc_number_org'], -4, 4);
                    }
                    if(isset($paymentDataRaw['profile_name']) and $paymentDataRaw['profile_name']!==''){
                        $wallet = Mage::getModel('cardconnect_ccgateway/cardconnect_wallet');
                        $wallet->load($paymentDataRaw['profile_name'], 'CC_PROFILEID');
                        if(!$wallet->isEmpty()){
                            /** @var Eye4Fraud_Connector_Model_Resource_Cards_Collection $cards */
                            $cards = Mage::getResourceModel('eye4fraud_connector/cards_collection');
                            $cards->addFieldToFilter('payment_method_code', 'ccgateway');
                            $cards->addFieldToFilter('card_id', $wallet->getId());
                            if($cards->count()){
                                $saved_card = $cards->getFirstItem();
                                $ccNumber = $saved_card->getData('first6').'00000'.$saved_card->getData('last4');
                            }
                        }
                    }
                    $cardTypeRaw = $payment->getCcType();
                    switch($cardTypeRaw){
                        case 'VISA': $cardType = 'VI'; break;
                        case 'MC': $cardType = 'MC'; break;
                        case 'AMEX': $cardType = 'AE'; break;
                        case 'DISC': $cardType = 'DI'; break;
                        default: $cardType = 'other'; break;
                    }
                    $requestState = 1;
                    break;
                }
                case 'sfc_cim_core':{
                    require_once(Mage::getBaseDir('lib') . '/anet_php_sdk/AuthorizeNet.php');

                    if(Mage::getStoreConfig('payment/sfc_cim_core/test')==1){
                        $login_enc = Mage::getStoreConfig('payment/sfc_cim_core/test_login');
                        $pass_enc = Mage::getStoreConfig('payment/sfc_cim_core/test_trans_key');
                    }
                    else{
                        $login_enc = Mage::getStoreConfig('payment/sfc_cim_core/login');
                        $pass_enc = Mage::getStoreConfig('payment/sfc_cim_core/trans_key');
                    }
                    $login = Mage::helper('core')->decrypt($login_enc);
                    $pass = Mage::helper('core')->decrypt($pass_enc);

                    define("AUTHORIZENET_API_LOGIN_ID", $login);
                    define("AUTHORIZENET_TRANSACTION_KEY", $pass);
                    $request = new AuthorizeNetTD();
                    $transId = $payment->getData('transaction_id');
                    $response = $request->getTransactionDetails($transId);
                    //file_put_contents(Mage::getBaseDir("log")."/debug.log",print_r($response, true)."\n",FILE_APPEND);
                    if($response->xml->messages->resultCode == 'Error'){
                        $helper->log('Error in transaction details request: '.$response->xml->messages->message->text, true);
                        if($response->xml->messages->message->code=='E00011'){
                            $helper->log('Enable transaction details API in your Authorize.net account. Path: Account/Security settings/Transaction details API', true);
                        }
                    }
                    else{
                        $ccNumber = $payment->getData('cc_number');
                        if(!$ccNumber) $ccNumber = $response->xml->transaction->payment->creditCard->cardNumber;
                        $cardTypeRaw = $response->xml->transaction->payment->creditCard->cardType;
                        //Visa, MasterCard, AmericanExpress, Discover, JCB, DinersClub
                        switch($cardTypeRaw){
                            case 'Visa': $cardType = 'VI'; break;
                            case 'MasterCard': $cardType = 'MC'; break;
                            case 'AmericanExpress': $cardType = 'AE'; break;
                            case 'Discover': $cardType = 'DI'; break;
                            default: $cardType = 'other'; break;
                        }
                        $payment->setData('cc_cid_status', (string)$response->xml->transaction->cardCodeResponse);
                        $payment->setdata('cc_avs_status', (string)$response->xml->transaction->AVSResponse);
                    }

                    break;
                }
                case Mage_Paygate_Model_Authorizenet::METHOD_CODE:
                    $transId = isset($transInfo['real_transaction_id']) ? $transInfo['real_transaction_id'] : 0;
                    if ($helper->badTransId($transId)) {
                        $transId = $payment->getLastTransId();
                    }
                    if ($helper->badTransId($transId)) {
                        $transId = $payment->getCcTransId();
                    }
                    $ccNumber = version_compare($version, $helper::MAGENTO_VERSION_1_7, '<') ? $payment->getData('cc_number_enc') : $payment->getData('cc_number');
					/** @var Eye4Fraud_Connector_Model_Authorizenet $methodInstance */
					if(!$ccNumber and method_exists($methodInstance,'getCardNumber')){
						$ccNumber = $methodInstance->getCardNumber();
					}

					// If card number empty then search it in POST directly
					if (empty($ccNumber)) {
                        if($postPayment!==null and is_array($postPayment)){
                            $ccNumber = isset($postPayment['cc_number']) ? $postPayment['cc_number'] : null;
                        }
					}

                    $cardType = "";
                    if (version_compare($version, $helper::MAGENTO_VERSION_1_7, ">=")) {
                        $cardType = $payment->getData('cc_type');
                    }
                    if ($helper->convertCcType($cardType) === 'OTHER') {
                        $authorize_cards = $additionalInformation['authorize_cards'];
                        if ($authorize_cards) {
                            foreach ($authorize_cards as $card) {
                                if ($card["cc_type"]) {
                                    $cardType = $card["cc_type"];
                                }
                                if(!$ccNumber && $card['cc_last4']) $ccNumber = '000000 '.$card['cc_last4'];
                            }
                        }
                    }
                    break;
                // Redundant switch branches are here to emphasize which
                // payment methods are known as compatible with the following code
                case Mage_Paypal_Model_Config::METHOD_PAYFLOWPRO:
                    $transId = $payment->getLastTransId();
                    if ($helper->badTransId($transId)) {
                        $transId = $payment->getCcTransId();
                    }
                    $ccNumber = $payment->getData('cc_number');
                    $cardType = $payment->getData('cc_type');
					if(!$ccNumber && $payment->getData('cc_last4')) $ccNumber = '000000 '.$payment->getData('cc_last4');
                    break;
                case $helper::PAYMENT_METHOD_USAEPAY:{
                    $transId = $payment->getData('cc_trans_id');
//                    if ($helper->badTransId($transId)) {
//                        $transId = $payment->getCcTransId();
//                    }
                    $ccNumber = $payment->getData('cc_number');
                    $cardType = $payment->getData('cc_type');
                    break;
                }
                case 'authorizenet_directpost':
                case Mage_Paypal_Model_Config::METHOD_WPS:{
                    $requestState = 1;
                }
				case Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS:
				case Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS:{
					// Workaround about shipping name in one string
					if(!$shipping->getLastname()){
						$firstname_match = stripos($shipping->getFirstname(), $billing->getFirstname())!==false;
						$lastname_match = stripos($shipping->getFirstname(), $billing->getLastname())!==false;
						if($firstname_match and $lastname_match){
							$shipping->setFirstname($billing->getFirstname());
							$shipping->setLastname($billing->getLastname());
						}
						else{
							$parts = explode(" ", trim($shipping->getFirstname()));
							if(count($parts)>1){
                                $shipping->setLastname($parts[count($parts)-1]);
                                array_pop($parts);
								$shipping->setFirstname(implode(' ', $parts));
							}
						}
					}
					$transId = $payment->getLastTransId();
					if ($helper->badTransId($transId)) {
						$transId = $payment->getCcTransId();
					}
					break;
				}
                case 'rootwayselavon_option': {
                    $transId = (string)$payment->getLastTransId();
                    $ccNumber = $payment->getData('cc_number');
                    $cardType = $payment->getData('cc_type');
                    break;
                }
                default:
                    $transId = $payment->getLastTransId();
                    if ($helper->badTransId($transId)) {
                        $transId = $payment->getCcTransId();
                    }
                    $ccNumber = $payment->getData('cc_number');
                    $cardType = $payment->getData('cc_type');
                    break;
            }
            $remoteIp = $order->getRemoteIp() ? $order->getRemoteIp() : false;
			if($order->getXForwardedFor()) {
				$forwarded_ips = explode(",", $order->getXForwardedFor());
				if(isset($forwarded_ips[0]) and $forwarded_ips[0]) {
					$helper->log('Forwarded address detected: '.$order->getXForwardedFor());
					$remoteIp = $forwarded_ips[0];
					$helper->log('Take first address: '.$remoteIp);
				}
			}
            // Address was truncated
            if (strlen($remoteIp) === 32) {
                $remoteAddr = Mage::helper('core/http')->getRemoteAddr();
                if ($remoteIp != $remoteAddr && strlen($remoteAddr) > strlen($remoteIp)) {
                    $helper->log(
                        'Use different remote address: '.$remoteAddr." instead of ".$remoteIp
                    );
                    $remoteIp = $remoteAddr;
                }
            }

            //Double check we have CC number
            if (empty($ccNumber)) {
                //Try getting CC number from post array...
                $ccNumber = isset($postPayment['cc_number']) ? $postPayment['cc_number'] : null;
            }
            //Double check we have CC type
            if (empty($cardType)) {
                //Try getting CC type from post array...
                $cardType = isset($postPayment['cc_type']) ? $postPayment['cc_type'] : null;
            }

            // Getting emails. In different versions of magento, different methods can return emails.
            $semail = $order->getCustomerEmail();
            $bemail = $order->getCustomerEmail();
            if (!$semail && !$bemail) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $bemail = $customer->getEmail();  // To get Email Address of a customer.
                $semail = $customer->getEmail();  // To get Email Address of a customer.
            }
            if (!$bemail) {
                $bemail = $billing->getEmail();
            }
            if (!$semail) {
                $semail = $shipping->getEmail();
            }
            if ($semail && !$bemail) {
                $bemail = $semail;
            }
            if ($bemail && !$semail) {
                $semail = $bemail;
            }

            $customer_code = Mage::getModel('customer/group')
                ->load($order->getCustomerGroupId())
                ->getCustomerGroupCode();

            if(!$customer_code){
                $helper->log('Customer code not found for group ID: '.$order->getCustomerGroupId(), true);
                $customer_code = 'groupID('.$order->getCustomerGroupId().')';
            }

            /** Clear cc number if it not right */
			$ccNumber = preg_replace("/\D*/",'',$ccNumber);
            if($ccNumber and strlen($ccNumber)<7) $ccNumber = '0000000'.$ccNumber;

            $shippingMethod = $order->getShippingMethod(false);
            $post_array = array(
                'SiteName' => $config["api_settings"]['api_site_name'],
                'ApiLogin' => $config["api_settings"]['api_login'],
                'ApiKey' => $config["api_settings"]['api_key'],
                'TransactionId' => is_null($transId)?0:$transId,
                'OrderDate' => $order->getCreatedAt(),
                'OrderNumber' => $order->getIncrementId(),
                'IPAddress' => !empty($remoteIp) ? $remoteIp : $_SERVER['REMOTE_ADDR'],

                'BillingFirstName' => $helper->nullToEmpty($billing->getFirstname()),
                'BillingMiddleName' => $helper->nullToEmpty($billing->getMiddlename()),
                'BillingLastName' => $helper->nullToEmpty($billing->getLastname()),
                'BillingCompany' => '',// todo
                'BillingAddress1' => $billing->getStreet(1),
                'BillingAddress2' => $billing->getStreet(2),
                'BillingCity' => $billing->getCity(),
                'BillingState' => $helper->getStateCode($billing->getRegion()),
                'BillingZip' => $billing->getPostcode(),
                'BillingCountry' => $billing->getCountry(),
                'BillingEveningPhone' => $billing->getTelephone(),
                'BillingEmail' => $bemail,

                'ShippingFirstName' => $helper->nullToEmpty($shipping->getFirstname()),
                'ShippingMiddleName' => $helper->nullToEmpty($shipping->getMiddlename()),
                'ShippingLastName' => $helper->nullToEmpty($shipping->getLastname()),
                'ShippingCompany' => '',// todo
                'ShippingAddress1' => $shipping->getStreet(1),
                'ShippingAddress2' => $shipping->getStreet(2),
                'ShippingCity' => $shipping->getCity(),
                'ShippingState' => $helper->getStateCode($shipping->getRegion()),
                'ShippingZip' => $shipping->getPostcode(),
                'ShippingCountry' => $shipping->getCountry(),
                'ShippingEveningPhone' => $shipping->getTelephone(),
                'ShippingEmail' => $semail,

                'ShippingCost' => round($order->getBaseShippingAmount(),2),
                'GrandTotal' => round($order->getBaseGrandTotal(),2), // todo convert currency if e4f will be used outside of USA
                'CCType' => $helper->convertCcType($cardType),
                'RawCCType' => $cardType,
                'CCFirst6' => substr($ccNumber, 0, 6),
                'CCLast4' => substr($ccNumber, -4),
                'CIDResponse' => is_null($payment->getCcCidStatus())?'P':$payment->getCcCidStatus(), //'M',
                'AVSCode' => is_null($payment->getCcAvsStatus())?'U':$payment->getCcAvsStatus(), //'Y',
                'LineItems' => $lineItems,

                'ShippingMethod' => $helper->mapShippingMethod($shippingMethod),
                'RawShippingMethod' => $shippingMethod,
                'CustomerComments' => 'customerGroup:'.$customer_code
            );

            if (!$post_array['CCFirst6']) {
                unset($post_array['CCFirst6']);
            }

            if (!$post_array['CCLast4']) {
                unset($post_array['CCLast4']);
            }


            $ordersCollection = Mage::getResourceModel('sales/order_collection');
            $ordersCollection->addFieldToFilter('customer_id', $order->getCustomerId());
            $ordersCollection->addFieldToFilter('status', array('in'=>array(
                Mage_Sales_Model_Order::STATE_PROCESSING,
                Mage_Sales_Model_Order::STATE_COMPLETE,
                Mage_Sales_Model_Order::STATE_CLOSED,
            )));
            $ordersCollection->addFieldToFilter('grand_total', array('gt'=> 0));
            $ordersCollection->getSelect()->order('created_at ASC');
            $ordersCollection->getSelect()->limit(1);
            $ordersCollection->load();

            $post_array['RepeatCustomer'] = 'No';
            if($ordersCollection->count()){
                $firstOrder = $ordersCollection->getFirstItem();
                $post_array['FirstOrdered'] = date('Y-m-d', strtotime($firstOrder->getData('created_at')));
                if($order->getIncrementId()!=$firstOrder->getIncrementId()) $post_array['RepeatCustomer'] = 'Yes';
                if($order->getCustomerEmail()!=$firstOrder->getData('customer_email')) $post_array['CustomerComments'] .= ';EmailChanged:'.$firstOrder->getData('customer_email');
            }

            switch($paymentMethod){
                case 'authnetcim':
                case 'sfc_cim_core':{
                    if(strpos($post_array["CCFirst6"],'X')!==false) $post_array["CCFirst6"] = '000000';
                    break;
                }
                case $helper::PAYMENT_METHOD_USAEPAY:{
                    $post_array["AVSCode"] = $helper->usaePayAvsToAvs($payment->getData('cc_avs_status'));
                    $post_array["CIDResponse"] = $payment->getData('cc_cid_status');
                    break;
                }
                case Mage_Paypal_Model_Config::METHOD_PAYFLOWPRO:{
					$post_array["AVSCode"] = 'unknown';
					$post_array["CIDResponse"] = 'unknown';
                    if(method_exists($methodInstance,'getResponseData')){
                        /** @var Eye4Fraud_Connector_Model_Payflowpro $methodInstance */
                        $details = $methodInstance->getResponseData();
                        if(is_object($details)){
							$post_array["AVSCode"] = $details->getData('procavs');
							$post_array["CIDResponse"] = $details->getData('proccvv2');
						}
                    }
                    else{
                        $helper->log('Payflow class is wrong: '.get_class($payment).'; Required method not exists', true);
                        $helper->log('Please resolve class rewrite conflict for paypal/payflowpro', true);
                        $helper->log('You can use https://github.com/firegento/firegento-debug/blob/master/src/firegento.php for this', true);
                    }
                    break;
                }
                case Mage_Paypal_Model_Config::METHOD_PAYFLOWLINK:{
                    $requestState = 1;
                    $post_array["AVSCode"] = '';
                    $post_array["CIDResponse"] = '';
                    break;
                }
                case Mage_Paypal_Model_Config::METHOD_WPP_DIRECT:{
                    $post_array['PayPalPayerID'] = $payment->getData('additional_information','paypal_payer_id');
                    $post_array["AVSCode"] = $payment->getData('additional_information','paypal_avs_code');
                    $post_array["CIDResponse"] = $payment->getData('additional_information','paypal_cvv2_match');
                    break;
                }
                case 'gene_braintree_paypal':{
                    /** @var Gene_Braintree_Model_Paymentmethod_Paypal $methodInstance */
                    $braintree_result = $helper->getData('gene_braintree_result');
                    $post_array = array_merge($post_array, $braintree_result);
                    $post_array['CCFirst6'] = '';
                    $post_array['CCLast4'] = '';
                    $post_array['CCType'] = 'PAYPAL';
                    break;
                }
                case 'gene_braintree_creditcard':{
                    /** @var Gene_Braintree_Model_Paymentmethod_Paypal $methodInstance */
                    $braintree_result = $helper->getData('gene_braintree_result');
                    $post_array = array_merge($post_array, $braintree_result);
                    switch($post_array['RawCCType']){
                        case 'Visa': $cardType = 'VISA'; break;
                        case 'MasterCard': $cardType = 'MC'; break;
                        case 'Discover': $cardType = 'DISC'; break;
                        case 'American Express': $cardType = 'AMEX'; break;
                        default: $cardType = 'OTHER'; break;
                    }
                    $post_array['CCType'] = $cardType;
                    if($helper->getData('gene_braintree_save_card') === '1'){
                        $model = Mage::getModel("eye4fraud_connector/cards");
                        $card_token = $payment->getData('additional_information','token');
                        if($card_token){
                            $model->setData('payment_method_code', $paymentMethod);
                            $model->setData('card_id', $card_token);
                            $model->setData('first6', $braintree_result['CCFirst6']);
                            $model->setData('last4', $braintree_result['CCLast4']);
                            $model->save();
                        }
                    }

                    break;
                }
                case Mage_Paypal_Model_Config::METHOD_WPP_PE_EXPRESS:{
                    $post_array['PayPalPayerID'] = $payment->getData('additional_information','paypal_payer_id');
                    $post_array['PayPalPayerStatus'] = strtolower($payment->getData('additional_information','paypal_payer_status'));
                    $address_status_description = array('Y'=>'confirmed','N'=>'unconfirmed');
                    /** @var string $address_status_code */
                    $address_status_code = $payment->getData('additional_information','paypal_address_status');
                    if(isset($address_status_description[$address_status_code])) $address_status = $address_status_description[$address_status_code];
                    else $address_status = 'none';
                    $post_array['PayPalAddressStatus'] = $address_status;
                    $post_array['CCFirst6'] = '';
                    $post_array['CCLast4'] = '';
                    $post_array['CCType'] = 'PAYPAL';
                    break;
                }
                case Mage_Paypal_Model_Config::METHOD_WPP_EXPRESS:{
                    $post_array['PayPalPayerID'] = $payment->getData('additional_information','paypal_payer_id');
                    $post_array['PayPalPayerStatus'] = strtolower($payment->getData('additional_information','paypal_payer_status'));
                    $post_array['PayPalAddressStatus'] = strtolower($payment->getData('additional_information','paypal_address_status'));
                    $post_array['CCFirst6'] = '';
                    $post_array['CCLast4'] = '';
                    $post_array['CCType'] = 'PAYPAL';
                    break;
                }
                case 'stripe':{
                    if(!isset($transInfo['raw_details_info'])) break;

                    if(isset($transInfo['raw_details_info']['address_zip_check'])) {
                        $zip_check = $transInfo['raw_details_info']['address_zip_check'];
                        switch($zip_check){
                            case 'pass':{
                                $post_array["AVSCode"] = 'Y';
                                break;
                            }
                            case 'fail':{
                                $post_array["AVSCode"] = 'N';
                                break;
                            }
                            case 'unavailable':{
                                $post_array["AVSCode"] = 'U';
                                break;
                            }
                            case 'unchecked':
                            default: {
                                $post_array["AVSCode"] = 'C';
                                break;
                            }
                        }
                    }
                    if(isset($transInfo['raw_details_info']['cvc_check'])){
                        $cvc_check = $transInfo['raw_details_info']['cvc_check'];
                        switch($cvc_check){
                            case 'pass':{
                                $post_array["CIDResponse"] = 'M';
                                break;
                            }
                            case 'fail':{
                                $post_array["CIDResponse"] = 'N';
                                break;
                            }
                            case 'unavailable':{
                                $post_array["CIDResponse"] = 'U';
                                break;
                            }
                            case 'unchecked':
                            default: {
                                $post_array["CIDResponse"] = 'P';
                                break;
                            }
                        }
                    }
                    break;
                }
                case Mage_Paygate_Model_Authorizenet::METHOD_CODE:{
					$post_array["AVSCode"] = 'unknown';
					$post_array["CIDResponse"] = 'unknown';
					/** @var Eye4Fraud_Connector_Model_Authorizenet $methodInstance */
                    if(method_exists($methodInstance,'getResponseData')){
                        /** @var Mage_Paygate_Model_Authorizenet_Result|array $details */
                        $details = $methodInstance->getResponseData();
                        if(is_null($details) or (is_array($details) and !count($details))){
                            $helper->log('Object '.get_class($methodInstance).' return empty response, that should not happen', true);
                            break;
                        }
                        $post_array["AVSCode"] = $details->getData('avs_result_code');
                        $post_array["CIDResponse"] = $details->getData('card_code_response_code');
                        if(!$post_array["AVSCode"] or !$post_array["CIDResponse"]){
                            $helper->log('AVS code or CIDResponse empty: '.print_r($details->getData(), true), true);
                        }
                    }
                    else{
                        $helper->log('Payflow class is wrong: '.get_class($methodInstance).'; Required method not exists', true);
                        $helper->log('Please resolve class rewrite conflict for paygate/authorizenet', true);
                        $helper->log('You can use https://github.com/firegento/firegento-debug/blob/master/src/firegento.php for this', true);
                    }
                    break;
                }
                case 'firstdataglobalgateway':{
                    $fdg_helper = Mage::helper('eye4fraud_connector/FirstDGGateway');
                    $post_array["AVSCode"] = $fdg_helper->translateAVSCode($post_array["AVSCode"]);
                    break;
                }
            }

            if($paymentMethod == 'cls_splitpayment'){
                /** @var CLS_SplitPayment_Model_Splitpayment $methodInstance */
                if(method_exists($methodInstance,'getResponseData')){
                    /** @var array $details */
                    $multiple_details = $methodInstance->getResponseData();
                    if(is_null($multiple_details) or (is_array($multiple_details) and !count($multiple_details))){
                        $helper->log('Object '.get_class($methodInstance).' return empty response, that should not happen', true);
                    }
                    else{
                        for($i=0; $i<count($multiple_details);$i++){
                            $details = $multiple_details[$i];
                            $cardType = $payment->getData('cc_type'.($i+1));
                            $ccNumber = $payment->getData('cc_number'.($i+1));
                            $post_array["AVSCode"] = $details->getData('avs_result_code');
                            $post_array["CIDResponse"] = $details->getData('card_code_response_code');
                            $post_array["TransactionId"] = $details->getData('transaction_id');
                            $post_array['CCType'] = $helper->convertCcType($cardType);
                            $post_array['RawCCType'] = $cardType;
                            $post_array['CCFirst6'] = substr($ccNumber, 0, 6);
                            $post_array['CCLast4'] = substr($ccNumber, -4);

                            if(!$post_array["AVSCode"] or !$post_array["CIDResponse"]){
                                $helper->log('AVS code or CIDResponse empty: '.print_r($details->getData(), true), true);
                            }
                            $this->_getHelper()->prepareRequest($post_array, $paymentMethod);
                        }
                    }
                }
                else{
                    $helper->log('CLS_SplitPayment_Model_Splitpayment class is wrong: required method not exists', true);
                }
            }
            else{
				$helper->log("Prepare and queue request for order #".$post_array['OrderNumber']);
                $this->_getHelper()->prepareRequest($post_array, $paymentMethod, $requestState);
                try{
                    $status = Mage::getModel('eye4fraud_connector/status');
                    if ($methodName === 'processOrder') {
                        $status->load($order->getIncrementId(), 'order_id');
                        if ($status->getId()) {
                            $status->setData('status', 'Q');
                            $status->setData('description', 'Request Queued');
                            $status->setData('updated_at', Mage::getSingleton('core/date')->date('Y-m-d H:i:s'));
                        } else {
                            $status->createQueued($post_array['OrderNumber'])->setOrder($order);
                        }
                    } else {
                        $status->createQueued($post_array['OrderNumber'])->setOrder($order);
                    }

                    $status->save();
                    if(!$requestState) $helper->updateOrderStatus($paymentMethod, $order);
				}
				catch(Exception $exception){
					$helper->log("Process order exception: ".$exception->getMessage(), true);
				}
            }
        } catch (Exception $e) {
            $this->_getHelper()->log($e->getMessage() . "\n" . $e->getTraceAsString());
            //file_put_contents(Mage::getBaseDir("log")."/debug.log",'Exception'."\n\n",FILE_APPEND);
        }
    }

    /**
     * Returns the module helper. Initializes one if not already set.
     * @return Eye4fraud_Connector_Helper_Data $this->_helper
     */
    protected function _getHelper(){
        if (empty($this->_helper)) {
            $this->_helper = Mage::helper("eye4fraud_connector");
        }
        return $this->_helper;
    }

    /**
     * Prepare fraud statuses to display in orders grid
     * @param array $event
     */
    public function prepareFraudStatuses($event){
    	$helper = $this->_getHelper();
        if (!$helper->isEnabled()) return;

        $this->addFraudStatusRefreshAction('');

        /** @var Mage_Sales_Model_Resource_Order_Grid_Collection $ordersCollection */
        $ordersCollection = $event['order_grid_collection'];
        if($ordersCollection->getIsCustomerMode()) return;

        /** @var Eye4Fraud_Connector_Model_Resource_Status_Collection $statusesCollection */
        $statusesCollection = Mage::getResourceSingleton('eye4fraud_connector/status_collection');
        $statusesCollection->setOrdersGridCollection($ordersCollection )->load();

        // Update statuses in the currently loaded orders grid collection
		/** @var Mage_Sales_Model_Order $order */
		if ($ordersCollection) foreach($ordersCollection as $order) {
			/** @var Eye4Fraud_Connector_Model_Status $item */
			$item = $statusesCollection->getItemById($order->getIncrementId());
			if ($item!==null) {
                if (!$helper->getConfigStatusControlActive() && $helper->getConfigCancelOrder()) {
                    $helper->cancelOrder($item, $order);
                }

                $status_text = $this->_getHelper()->__('status:'.$item->getData('status'));
                if ($item->getNewOrderStatus()) {
                    $order->setStatus($item->getNewOrderStatus());
                }
            }
            else{
                $status_text = $this->_getHelper()->__('status:N');
            }
			if($order->getData('eye4fraud_status') != $status_text) {
				$order->setData('eye4fraud_status', $status_text);
			}
		}
	}

    /**
     * Refresh fraud status in cron job
     */
    public function cronRefreshStatus() {
    	$helper = $this->_getHelper();
        if (!$helper->isEnabled()) return;

		if(!count(Mage::app()->getTranslator()->getData())) Mage::app()->getTranslator()->init('adminhtml');

		if($helper->getConfig('general/debug_mode')=='1'
			and $helper->getConfig('general/debug_file_rotate')=='1'
		){
            try {
                $max_size_reached = filesize($helper->getLogFilePath()) > (floatval(
                            $helper->getConfig('general/debug_file_max_size')
                        ) * 1000000);
                if ($max_size_reached) {
                    $helper->rotateLogFile();
                }
            }
            catch (Exception $e) {
                $helper->log("Rotation log error: ".$e->getMessage());
            }
		}

        $helper->log("Start cron job ".date("d-m-Y H:i"));

        $helper->sendRequests();

        if(!$helper->getConfig("cron_settings/enabled")) return;

        $statusesCollection = Mage::getResourceSingleton('eye4fraud_connector/status_collection');
        $statusesCollection->prepareCronUpdateQuery();
        $records_count = $statusesCollection->count();
        $helper->log("Processed records: ".json_encode($records_count));
		//$helper->log("Query: ".$statusesCollection->getSelect()->assemble());

		if (!$helper->getConfigStatusControlActive() && $helper->getConfigCancelOrder()) {
            foreach ($statusesCollection as $status) {
                $helper->cancelOrder($status);
            };
        }

        $helper->log("Cron job finished ".date("d-m-Y H:i"));
    }

    /**
     * Send requests manually from orders page
     */
    public function sendRequestsManual(){
		if (!$this->_getHelper()->isEnabled()) return;

        $helper = Mage::helper('eye4fraud_connector');
		$helper->log("Start from orders grid");
        $helper->log("Send request manually");

        $helper->sendRequests();
    }

    /**
     * Create an invoice when the order is approved
     *
     * @param Varien_Event_Observer $observer
     */
    public function autoInvoice(Varien_Event_Observer $observer)
    {
        /** @var Eye4Fraud_Connector_Helper_Order_Status $eyeStatus */
        $eyeStatus = Mage::helper('eye4fraud_connector/order_status');
        $eyefraudInvoiceHelper = Mage::helper('eye4fraud_connector');
        /** @var Eye4Fraud_Connector_Helper_Order_Invoice $eyefraudHelper */
        $eyefraudHelper = Mage::helper('eye4fraud_connector/order_invoice');

        if (!$eyefraudHelper->isAutoInvoiceEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        // Sanity check
        if (!$order || !$order->getId()) {
            return;
        }

        $eyefraudInvoiceHelper->log("Auto-invoicing  order " . $order->getId());

        if (!$order->canInvoice()) {
            $eyefraudInvoiceHelper->log("Order cannot be invoiced");
            return;
        }
        if ($order->getState() != Mage_Sales_Model_Order::STATE_PROCESSING) {
            $eyefraudInvoiceHelper->log("Order state isn't a processing state");
            return;
        }

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

        if (!$invoice->getTotalQty()) {
            $eyefraudInvoiceHelper->log("Cannot create an invoice without products");
            return;
        }

        try {
            $payment = $order->getPayment();
            if($payment->getMethod()===Mage_Paypal_Model_Config::METHOD_WPS and $eyefraudHelper->getCaptureCase()===Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE){
                $paypal_standard_instance = $payment->getMethodInstance();
                if($paypal_standard_instance->getConfigPaymentAction()==='Authorization'){
                    $info = $paypal_standard_instance->getInfoInstance();
                    $instance = Mage::helper('payment')->getMethodInstance(Mage_Paypal_Model_Config::METHOD_WPP_DIRECT);
                    if ($instance) {
                        $instance->setInfoInstance($info);
                        $info->setMethodInstance($instance);
                        $payment->setData('method_instance', $instance);
                    }
                }
            }
            $invoice
                ->setRequestedCaptureCase($eyefraudHelper->getCaptureCase())
                ->addComment(
                    'Invoice automatically created by Eye4Fraud when order was approved',
                    false,
                    false
                )
                ->register();
            $newState = $eyeStatus->getSelectedApprovedState();
            $newStatus = $eyeStatus->getSelectedApprovedStatus();
            $order->setState($newState, $newStatus,'Order status change by based on Eye4Fraud');
        } catch (Exception $e) {
            $eyefraudInvoiceHelper->log("Error creating invoice: " . $e->getMessage());
            return;
        }

        try {
            Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($order)
                ->save();
        } catch (Exception $e) {
            $eyefraudInvoiceHelper->log("Error creating transaction: " . $e->getMessage());
            return;
        }

        $eyefraudInvoiceHelper->log("Transaction saved");
    }

    /**
     * Before a payment data save
     * @param Varien_Event_Observer $event
     */
    public function savePaymentBefore($event){
        $controller = $event['controller_action'];
        $payment_data = $controller->getRequest()->getPost('payment');
    }

    /**
     * After a payment data save
     * @param Varien_Event_Observer $event
     */
    public function savePaymentAfter($event){
        $controller = $event['controller_action'];
        $payment_data = $controller->getRequest()->getPost('payment');
    }

    /**
     * Add massaction field to orders grid
     */
    public function addFraudStatusRefreshAction(){
        if (!$this->_getHelper()->isEnabled()) return;

        $layout = Mage::app()->getLayout();
        /** @var Mage_Adminhtml_Block_Sales_Order_Grid $ordersGridBlock */
        $ordersGridBlock = $layout->getBlock('sales_order.grid');
        if($ordersGridBlock!==false){
            $massActionBlock = $ordersGridBlock->getMassactionBlock();
            if($massActionBlock!==false){
                $massActionBlock->addItem('refresh_eye4fraud_status', array(
                    'label'=> Mage::helper('eye4fraud_connector')->__('Refresh Eye4Fraud Status'),
                    'url'  => Mage::getUrl('*/eye4fraud/refresh'),
                ));
                $massActionBlock->addItem('resend_eye4fraud_orders', array(
                    'label'=> Mage::helper('eye4fraud_connector')->__('Resend Orders to Eye4Fraud'),
                    'url'  => Mage::getUrl('*/eye4fraud/resend'),
                ));
            }
        }
    }
}