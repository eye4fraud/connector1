<?php
/**
 * Observer for PayPal
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer_PayPal{

    /**
     * @param Varien_Event_Observer $event
     * @throws Exception
     */
    public function processIpnRequest($event){
        /** @var Mage_Paypal_IpnController $controller */
        $controller = $event['controller_action'];
        $helper = Mage::helper('eye4fraud_connector');

        $orderId = $controller->getRequest()->getParam('invoice');
        $helper->log('Processing PayPal IPN, #'.$orderId);
        //$helper->log($controller->getRequest()->getParams());

        /** @var Eye4Fraud_Connector_Model_Request $request */
        $request = Mage::getModel('eye4fraud_connector/request');
        $request->load($orderId,'increment_id');
        if($request->isEmpty()) {
            $helper->log('Cant load cached request from eye4fraud_requests_cache with increment_id='.$orderId);
            return;
        }
        if($request->getData('payment_method')!==Mage_Paypal_Model_Config::METHOD_WPS){
            $helper->log('Payment method is not '.Mage_Paypal_Model_Config::METHOD_WPS);
            return;
        }

        $order = $this->getOrder($request->getData('increment_id'));
        if($order->getState()!==Mage_Sales_Model_Order::STATE_PROCESSING){
            $helper->log('Order state is "'.$order->getState().'" not math to "processing"');
            return;
        }

        $post_array = unserialize($request->getData('request_data'));
        $post_array['TransactionId'] = $controller->getRequest()->getParam('txn_id');
        $post_array['PayPalPayerID'] = $controller->getRequest()->getParam('payer_id');
        $post_array['PayPalPayerStatus'] = $controller->getRequest()->getParam('payer_status');
        $post_array['PayPalAddressStatus'] = $controller->getRequest()->getParam('address_status');
        $request->setData('request_data', serialize($post_array));
        $request->setData('hold', 0);
        $request->save();
        $helper->log('Unhold cached request for order #'.$orderId);

        $this->updateOrderStatus($order);
        $order->save();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
    protected function updateOrderStatus($order){
        $autoSync = Mage::helper('eye4fraud_connector');
        if (!$autoSync->getConfigStatusControlActive()) {
            return;
        }
        $helper = Mage::helper('eye4fraud_connector');

        try{
            /** @var Eye4Fraud_Connector_Helper_Order_Status $eyeStatus */
            $eyeStatus = Mage::helper('eye4fraud_connector/order_status');
            $newState = $eyeStatus->getSelectedCreateOrderState();
            $newStatus = $eyeStatus->getSelectedCreateOrderStatus();
            if ($newState === Mage_Sales_Model_Order::STATE_HOLDED) {
                $this->holdOrder($order, $newStatus, 'Order status changed to Hold by  Eye4Fraud');
            } else {
                $order->setData('state', $newState);
                $order->setStatus($newStatus);
                $order->addStatusHistoryComment('Order status changed to Hold by  Eye4Fraud.',
                    false);
            }
        }
        catch (Exception $e){
            $helper->log('Error while trying to change order status #'.$order->getIncrementId());
            $helper->log($e->getMessage());
        }
    }

    /**
     * Load order
     * @param string $increment_id
     * @return Mage_Sales_Model_Order
     */
    protected function getOrder($increment_id){
        return Mage::getModel('sales/order')->loadByIncrementId($increment_id);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param bool $status
     * @param string $message
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function holdOrder($order, $status = true, $message = '')
    {
        if (!$order->canHold()) {
            Mage::throwException(Mage::helper('sales')->__('Hold action is not available.'));
        }
        $order->setHoldBeforeState($order->getState());
        $order->setHoldBeforeStatus($order->getStatus());
        $order->setState(Mage_Sales_Model_Order::STATE_HOLDED, $status, $message);
        return $this;
    }

}