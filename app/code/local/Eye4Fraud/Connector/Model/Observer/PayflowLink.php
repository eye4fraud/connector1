<?php
/**
 * Catch response from PayPal Payflow link
 * @author Mikhail Valiushka @2018
 */

class Eye4Fraud_Connector_Model_Observer_PayflowLink
{

    /**
     * When a customer return to website from payflow gateway.
     * @param Varien_Object $event
     */
    public function postSilentPostAction($event)
    {
        /** @var Mage_Paypal_PayflowController $controller */
        $controller = $event->controller_action;

        $helper = Mage::helper('eye4fraud_connector');
        $orderId = $controller->getRequest()->getParam('INVNUM');
        if ($orderId === null) {
            $helper->log('Response from PayPal Payflow link');
            $helper->log(print_r($controller->getRequest()->getParams(), true));
            return;
        }


        /** @var Eye4Fraud_Connector_Model_Request $request */
        $request = Mage::getModel('eye4fraud_connector/request');
        $request->load($orderId, 'increment_id');
        if ($request->isEmpty()) {
            $helper->log('Cant load cached request from eye4fraud_requests_cache with increment_id=' . $orderId);
            return;
        }

        $requestData = unserialize($request->getData('request_data'));
        list($shippingFirstname, $shippingLastname) = explode(" ", $controller->getRequest()->getParam('NAMETOSHIP'),
            2);
        $changedData = array(
            'TransactionId' => $controller->getRequest()->getParam('PNREF'),

            'BillingFirstName' => $controller->getRequest()->getParam('BILLTOFIRSTNAME'),
            'BillingLastName' => $controller->getRequest()->getParam('BILLTOLASTNAME'),
            'BillingAddress1' => $controller->getRequest()->getParam('BILLTOSTREET'),
            'BillingCity' => $controller->getRequest()->getParam('BILLTOCITY'),
            'BillingState' => $controller->getRequest()->getParam('BILLTOSTATE'),
            'BillingZip' => $controller->getRequest()->getParam('BILLTOZIP'),
            'BillingCountry' => $controller->getRequest()->getParam('BILLTOCOUNTRY'),
            'BillingEmail' => $controller->getRequest()->getParam('BILLTOEMAIL'),

            'CCLast4' => $controller->getRequest()->getParam('ACCT'),
            'CIDResponse' => $controller->getRequest()->getParam('PROCCVV2'),
            'ZipMatch' => $controller->getRequest()->getParam('AVSZIP'),
            'StreetMatch' => $controller->getRequest()->getParam('AVSADDR'),
            'CscMatch' => $controller->getRequest()->getParam('CVV2MATCH'),
        );
        if ($controller->getRequest()->getParam('SHIPTOSTREET') !== null) {
            $changedData['ShippingFirstName'] = $controller->getRequest()->getParam('FIRSTNAME');
            $changedData['ShippingLastName'] = $controller->getRequest()->getParam('LASTNAME');
            $changedData['ShippingAddress1'] = $controller->getRequest()->getParam('SHIPTOSTREET');
            $changedData['ShippingCity'] = $controller->getRequest()->getParam('SHIPTOCITY');
            $changedData['ShippingState'] = $controller->getRequest()->getParam('SHIPTOSTATE');
            $changedData['ShippingZip'] = $controller->getRequest()->getParam('SHIPTOZIP');
            $changedData['ShippingCountry'] = $controller->getRequest()->getParam('SHIPTOCOUNTRY');
        }

        if ($controller->getRequest()->getParam('EMAILTOSHIP')) {
            $changedData['ShippingEmail'] = $controller->getRequest()->getParam('EMAILTOSHIP');
        }

        $requestData = array_merge($requestData, $changedData);

        $request->setData('request_data', serialize($requestData));
        $request->setData('hold', 0);
        try {
            $request->save();
        } catch (Exception $e) {
            $helper->log('Error while saving updated request');
            $helper->log($e->getMessage());
            return;
        }

        $helper->log('Unhold cached request for order #' . $orderId);

    }

    /**
     * Paypal payflow link post action for returnUrlAction
     * @param Varien_Object $event
     */
    public function postReturnUrlAction($event)
    {
        /** @var Mage_Paypal_PayflowController $controller */
        $controller = $event->controller_action;

        $helper = Mage::helper('eye4fraud_connector');
        $orderId = $controller->getRequest()->getParam('INVNUM');
        if ($orderId === null) {
            $helper->log('Response from PayPal Payflow link');
            $helper->log(print_r($controller->getRequest()->getParams(), true));
            return;
        }

        if ($helper->getConfigStatusControlActive()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

            if ($order->getIncrementId() == $orderId) {
                $helper->updateOrderStatus(Mage_Paypal_Model_Config::METHOD_PAYFLOWLINK . '_allow', $order);
                if ($order->hasDataChanges()) {
                    try {
                        $order->save();
                    } catch (Exception $e) {
                        $helper->log('Error while saving order when it state was changed');
                        $helper->log($e->getMessage());
                    }
                }
            }
        }
    }

}