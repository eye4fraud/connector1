<?php
/**
 * Observer for CardConnect payment extension
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer_Ccgateway{

    public function ProcessPaymentResult(){
        $helper = Mage::helper('eye4fraud_connector');
        $session = Mage::getSingleton('checkout/session');
        $orderId = $session->getLastRealOrderId();
        $helper->log('Processing postdispatch for CardConnect payment method, #'.$orderId);

        $paymentDetails = Mage::getModel('cardconnect_ccgateway/cardconnect_resp');
        $paymentDetails->load($orderId, 'CC_ORDERID');
        if($paymentDetails->isEmpty()) {
            $helper->log('Cant load payment details from cardconnect_resp with CC_ORDERID='.$orderId);
            return;
        }

        /** @var Eye4Fraud_Connector_Model_Request $request */
        $request = Mage::getModel('eye4fraud_connector/request');
        $request->load($orderId,'increment_id');
        if($request->isEmpty()) {
            $helper->log('Cant load cached request from eye4fraud_requests_cache with increment_id='.$orderId);
            return;
        }
        $post_array = unserialize($request->getData('request_data'));
        $post_array['AVSCode'] = $paymentDetails->getData('CC_AVSRESP');
        $post_array['CIDResponse'] = $paymentDetails->getData('CC_CVVRESP');
        $post_array['TransactionId'] = $paymentDetails->getData('CC_RETREF');
        $request->setData('request_data', serialize($post_array));
        $request->setData('hold', 0);
        $request->save();

    }

    /**
     * Update saved card data on card update from customer profile
     * @param Varien_Event_Observer $event
     */
    public function UpdateCard($event){
        /** @var Cardconnect_Ccgateway_CardmanagementController $controller */
        $controller = $event['controller_action'];
        $data = $controller->getRequest()->getPost('editcard', array());
        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
        $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
        $collection->addFieldToFilter('payment_method_code', 'ccgateway')->addFieldToFilter('card_id', $data['wallet_id']);
        $collection->load();
        if($collection->count()) $model = $collection->getFirstItem();
        else{
            $model = Mage::getModel("eye4fraud_connector/cards");
            $model->setData('payment_method_code', 'ccgateway');
            $model->setData('card_id', $data['wallet_id']);
        }
        $card_number = preg_replace("/\D*/",'', $data['cc_number_org']);
        if(strlen($card_number)<10) return;

        $model->setData('first6', substr($card_number, 0,6));
        $model->setData('last4', substr($card_number, -4,4));
        $model->save();
    }

    /**
     * Delete saved card data
     * @param Varien_Event_Observer $event
     */
    public function DeleteCard($event){
        /** @var Cardconnect_Ccgateway_CardmanagementController $controller */
        $controller = $event['controller_action'];
        $card_id = $controller->getRequest()->getPost('cc_id');
        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
        $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
        $collection->addFieldToFilter('payment_method_code', 'ccgateway')->addFieldToFilter('card_id', $card_id);
        $collection->load();
        if($collection->count()) {
            /** @var Eye4Fraud_Connector_Model_Cards $model */
            $model = $collection->getFirstItem();
            $model->delete();
        }
    }

    /**
     * Create or update saved card
     * @param Varien_Event_Observer $event
     */
    public function UpdateWallet($event){
        $data = Mage::app()->getRequest()->getPost('payment');
        if($data===null) $data = Mage::app()->getRequest()->getPost('addcard');
        /** @var Eye4Fraud_Connector_Model_Cardconnect_Wallet $wallet */
        $wallet = $event['object'];
        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
        $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
        $collection->addFieldToFilter('payment_method_code', 'ccgateway')->addFieldToFilter('card_id', $wallet->getId());
        $collection->load();
        if($collection->count()) $model = $collection->getFirstItem();
        else{
            $model = Mage::getModel("eye4fraud_connector/cards");
            $model->setData('payment_method_code', 'ccgateway');
            $model->setData('card_id', $wallet->getId());
            $model->isObjectNew(true);
        }

        if(!isset($data['first6']) or !isset($data['cc_number_org'])) return;

        $model->setData('first6', $data['first6']);
        $model->setData('last4', substr($data['cc_number_org'], -4,4));
        $model->save();
    }
}