<?php
/**
 * Observer for Authorize.Net Direct post payment extension
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer_AuthnetDirectPost
{

    /**
     * @param Varien_Event_Observer $event
     * @throws Exception
     */
    public function response($event)
    {
        /** @var Mage_Authorizenet_Directpost_PaymentController $controller */
        $controller = $event['controller_action'];
        $helper = Mage::helper('eye4fraud_connector');

        $orderId = $controller->getRequest()->getParam('x_invoice_num');
        $helper->log('Processing AuthorizeNet DirectPost response, #'.$orderId);

        if((int)$controller->getRequest()->getParam('x_response_code') !== Mage_Paygate_Model_Authorizenet::RESPONSE_CODE_APPROVED){
            $helper->log('Transaction not approved, code '.$controller->getRequest()->getParam('x_response_code'));
            return;
        }

        /** @var Eye4Fraud_Connector_Model_Request $request */
        $request = Mage::getModel('eye4fraud_connector/request');
        $request->load($orderId,'increment_id');
        if($request->isEmpty()) {
            $helper->log('Cant load cached request from eye4fraud_requests_cache with increment_id='.$orderId);
            return;
        }
        if($request->getData('payment_method') !== 'authorizenet_directpost'){
            $helper->log('Payment method is not '.Mage_Paypal_Model_Config::METHOD_WPS);
            return;
        }

        $requestData = unserialize($request->getRequestData());
        $requestData['AVSCode'] = $controller->getRequest()->getPost('x_avs_code');
        $requestData['TransactionId'] = $controller->getRequest()->getPost('x_trans_id');
        $requestData['CCType'] = $controller->getRequest()->getPost('x_card_type');
        $requestData['RawCCType'] = $controller->getRequest()->getPost('x_card_type');
        $requestData['CCLast4'] = substr($controller->getRequest()->getPost('x_account_number'), -4);
        $requestData['CCFirst6'] = '000000';
        $requestData['CIDResponse'] = $controller->getRequest()->getPost('x_cvv2_resp_code');

        $request->setRequestData(serialize($requestData));
        $request->setHold(0);

        $request->save();

        /* @var $order Mage_Sales_Model_Order */
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        $helper->updateOrderStatus('authorizenet_directpost', $order);

        $order->save();
    }

}