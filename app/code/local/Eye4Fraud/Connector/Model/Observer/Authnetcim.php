<?php
/**
 * Observer for ParadoxLabs Authorize.Net CIM payment extension
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer_Authnetcim{

    /**
     * Create or update saved card
     * @param Varien_Event_Observer $event
     */
    public function UpdateWallet($event){
        $data = Mage::app()->getRequest()->getPost('payment');

        /** @var Eye4Fraud_Connector_Model_Authnetcim_Card $card */
        $card = $event['object'];
        /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
        $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
        $collection->addFieldToFilter('payment_method_code', $card->getData('method'))->addFieldToFilter('card_id', $card->getId());
        $collection->load();
        if($collection->count()) $model = $collection->getFirstItem();
        else{
            $model = Mage::getModel("eye4fraud_connector/cards");
            $model->setData('payment_method_code', $card->getData('method'));
            $model->setData('card_id', $card->getId());
            $model->isObjectNew(true);
        }

        if($card->getData('active')===0){
            $model->delete();
        }
        if(!isset($data['cc_number']) or strlen($data['cc_number'])<12) return;
        $cc_number = preg_replace("/\D*/",'',$data['cc_number']);
        $first6 = substr($cc_number,0,6);
        $last4 = substr($cc_number, -4, 4);
        if(!$first6 or !$last4) return;

        $model->setData('first6', $first6);
        $model->setData('last4', $last4);
        $model->save();
    }
}