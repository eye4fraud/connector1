<?php
/**
 * Observer for Gene Braintree payment extension
 *
 * @category    Eye4fraud
 * @package     Eye4fraud_Connector
 */

class Eye4Fraud_Connector_Model_Observer_GeneBraintree
{

    /**
     * Create or update saved card
     * @param Varien_Event_Observer $event
     */
    public function SavePayPalData($event)
    {
        $result = $event->getData('result');
        $helper = Mage::helper('eye4fraud_connector');

        $data = array(
            'PayPalPayerID' => $result->transaction->paypal['payerId'],
            'PayPalPayerStatus' => strtolower($result->transaction->paypal['payerStatus']),
        );

        $helper->setData('gene_braintree_result', $data);
    }


    /**
     * Create or update saved card
     * @param Varien_Event_Observer $event
     */
    public function SaveCardData($event)
    {
        $helper = Mage::helper('eye4fraud_connector');
        try{
            $result = $event->getData('result');
            $payment = $event->getData('payment');

            $data = array(
                'CCFirst6' => $result->transaction->creditCard['bin'],
                'CCLast4' => $result->transaction->creditCard['last4'],
                'RawCCType' => $result->transaction->creditCard['cardType'],
                'CIDResponse' => $result->transaction->cvvResponseCode,
                'AVSCode' => $result->transaction->avsPostalCodeResponseCode,
                //'AVSCode' => $result->transaction->avsStreetAddressResponseCode,
            );
            $helper->setData('gene_braintree_result', $data);

            $helper->setData('gene_braintree_save_card', $payment->getData('additional_information', 'save_card'));

            $usedCard = $payment->getData('additional_information', 'card_payment_method_token');
            $helper->setData('gene_braintree_used_card', $usedCard);
        }
        catch (Exception $e){
            $helper->log('Error in SaveCardDate for GeneBraintree:');
            $helper->log($e->getMessage());
        }
    }

    /**
     * Remove card from remembered
     * @param $event
     */
    public function RemoveCard($event)
    {
        $helper = Mage::helper('eye4fraud_connector');
        /** @var Gene_Braintree_SavedController $controller */
        $controller = $event['controller_action'];
        $token = $controller->getRequest()->getParam('id');
        if ($token) {
            /** @var Mage_Core_Model_Resource_Db_Collection_Abstract $collection */
            $collection = Mage::getModel("eye4fraud_connector/cards")->getCollection();
            $collection->addFieldToFilter('payment_method_code', 'gene_braintree_creditcard')->addFieldToFilter('card_id', $token);
            $collection->setPageSize(1)->setCurPage(1);
            $collection->load();
            if ($collection->count()) {
                /** @var Eye4Fraud_Connector_Model_Cards $model */
                $model = $collection->getFirstItem();
                try {
                    $model->delete();
                }
                catch (Exception $e) {
                    $helper->log('Error while remove card with ID: '.$token);
                }
            }
        }
    }


}