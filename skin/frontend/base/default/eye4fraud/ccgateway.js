/**
 * Override CardConnect method to get first 6 digits for CC
 */

(function($){
    "use strict";
    $(function(){
        var tObject = null;
        var originalFunction = null;
        var $paymentForm = [];
        var $first6digits = null;

        var newFunction = function(value, isTestMode, siteName, errorLogURL, saveUrl, reviewUrl, failureUrl, guest){
            if($first6digits !== null) $first6digits.val(value.substr(0,6));
            originalFunction(value, isTestMode, siteName, errorLogURL, saveUrl, reviewUrl, failureUrl, guest);
        };

        var fCatch = function(){
            if(!$paymentForm.length && ($paymentForm = $('#payment_form_ccgateway')).length){
                console.log('eye4fraud: Payment form found, adding field');
                $first6digits = $('<input type="hidden" name="payment[first6]" id="cc_gateway_first6">');
                $paymentForm.prepend($first6digits);
            }
            if(!$paymentForm.length && ($paymentForm = $('#add_new_card_form_ccgateway')).length){
                console.log('eye4fraud: Payment form found, adding field');
                $first6digits = $('<input type="hidden" name="addcard[first6]" id="cc_gateway_first6">');
                $paymentForm.prepend($first6digits);
            }

            if(originalFunction === null && typeof(window['valid_credit_card'])!=='undefined'){
                console.log('eye4fraud: Replace function "valid_credit_card"');
                originalFunction = window['valid_credit_card'];
                window['valid_credit_card'] = newFunction;
            }

            if(originalFunction !== null && $paymentForm.length){
                console.log('eye4fraud: All injections finished');
                clearInterval(tObject);
            }
        };
        tObject = setInterval(fCatch, 1000);
    });
})(jQuery);