<!doctype html>
<head>
	<title>Remove files of Eye4Fraud extension</title>
</head>
<body>
<h1>Remove files of Eye4Fraud extension</h1>
<?php
/**
 * Remove extension files
 * @author Mikhail Valiushka
 */

define("_PATH_",realpath(dirname(__FILE__)));

$remove_items = array(
	_PATH_.'/app/code/local/Eye4Fraud',
	_PATH_.'/app/design/adminhtml/default/default/layout/eye4fraud',
	_PATH_.'/app/etc/modules/Eye4Fraud_Connector.xml',
	_PATH_.'/app/locale/en_US/Eye4Fraud_Connector.csv'
);

foreach($remove_items as $item){
	if(!file_exists($item)){
		echo "<div>Path not found: ".$item."</div>";
		continue;
	}

	if(is_dir($item)) {
		echo "<div>Found directory: ".$item."</div>";
		rrmdir($item);
		if(file_exists($item)){
			echo "<div style='color: darkred'>Directory wasn't removed</div>";
		}
		else{
			echo "<div style='color: darkgreen'>Directory removed</div>";
		}
	}
	else{
		echo "<div>Found file: ".$item."</div>";
		unlink($item);
		if(file_exists($item)){
			echo "<div style='color: darkred'>File wasn't removed</div>";
		}
		else{
			echo "<div style='color: darkgreen'>File removed</div>";
		}
	}
}

echo '<h3>Finished!</h3>';

function rrmdir($dir) {
	if (is_dir($dir)) {
		$objects = scandir($dir);
		foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
				if (is_dir($dir."/".$object))
					rrmdir($dir."/".$object);
				else
					unlink($dir."/".$object);
			}
		}
		rmdir($dir);
	}
}
?>
</body>